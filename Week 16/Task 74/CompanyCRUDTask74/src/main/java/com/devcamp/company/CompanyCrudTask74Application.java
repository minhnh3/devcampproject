package com.devcamp.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanyCrudTask74Application {

	public static void main(String[] args) {
		SpringApplication.run(CompanyCrudTask74Application.class, args);
	}

}
