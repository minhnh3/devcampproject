/*
 Navicat Premium Data Transfer

 Source Server         : IRONHACK_LP DEV
 Source Server Type    : MySQL
 Source Server Version : 100508
 Source Host           : 42.115.221.44:3307
 Source Schema         : pizza365

 Target Server Type    : MySQL
 Target Server Version : 100508
 File Encoding         : 65001

 Date: 14/05/2021 16:57:33
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence`  (
  `next_val` bigint
) ENGINE = InnoDB;
-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES (10);

-- ----------------------------
-- Table structure for phong_ban
-- ----------------------------
DROP TABLE IF EXISTS `phong_ban`;
CREATE TABLE `phong_ban`  (
  `id` bigint(20) NOT NULL,
  `gioi_thieu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ma_phong_ban` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nghiep_vu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ten_phong_ban` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE (`ma_phong_ban`),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of phong_ban
-- ----------------------------
INSERT INTO `phong_ban` VALUES (1, NULL, 'MAR', 'Nghiên cứu thị trường - Định hướng sản phẩm - Truyền thông quảng cáo', 'Marketing');
INSERT INTO `phong_ban` VALUES (2, NULL, 'PROD', 'Sản xuất sản phẩm', 'Sản xuất');
INSERT INTO `phong_ban` VALUES (3, NULL, 'SALE', 'Bán sản phẩm', 'Kinh doanh');
INSERT INTO `phong_ban` VALUES (4, NULL, 'HR', 'Phát triển nguồn nhân lực', 'Nhân sự');

-- ----------------------------
-- Table structure for nhan_vien
-- ----------------------------
DROP TABLE IF EXISTS `nhan_vien`;
CREATE TABLE `nhan_vien`  (
  `id` bigint(20) NOT NULL,
  `chuc_vu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dia_chi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gioi_tinh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ma_nhan_vien` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ngay_sinh` datetime NOT NULL,
  `so_dien_thoai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ten_nhan_vien` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phong_ban_id` bigint(20) NOT NULL,
  CONSTRAINT `UK_nhan_vien` UNIQUE (`ma_nhan_vien`,`so_dien_thoai`),
  PRIMARY KEY (`id`) USING BTREE,
  CONSTRAINT `FK_phong_ban_nhan_vien` FOREIGN KEY (`phong_ban_id`) REFERENCES `phong_ban`(`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of nhan_vien
-- ----------------------------
INSERT INTO `nhan_vien` VALUES (1, 'Quản lý', 'diaChi mar1', 'Nam', 'MAR_NV_001', '1111-11-11 07:00:00', '0000000001', 'tenNhanVien mar1', 1);
INSERT INTO `nhan_vien` VALUES (2, 'Thiết kế', 'diaChi mar2', 'Nữ', 'MAR_NV_002', '1112-11-11 07:00:00', '0000000002', 'tenNhanVien mar2', 1);
INSERT INTO `nhan_vien` VALUES (3, 'Truyền thông', 'diaChi mar3', 'Nữ', 'MAR_NV_003', '1113-11-11 07:00:00', '0000000003', 'tenNhanVien mar3', 1);
INSERT INTO `nhan_vien` VALUES (4, 'Quản lý', 'diaChi prod1', 'Nam', 'PROD_NV_001', '2111-11-11 07:00:00', '0000000021', 'tenNhanVien prod1', 2);
INSERT INTO `nhan_vien` VALUES (5, 'Sản xuất', 'diaChi prod2', 'Nam', 'PROD_NV_002', '2112-11-11 07:00:00', '0000000022', 'tenNhanVien prod2', 2);
INSERT INTO `nhan_vien` VALUES (6, 'Kinh doanh', 'diaChi sale1', 'Nam', 'SALE_NV_001', '3111-11-11 07:00:00', '0000000031', 'tenNhanVien sale1', 3);

SET FOREIGN_KEY_CHECKS = 1;
