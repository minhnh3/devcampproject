"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// URL get API
const gBASE_URL = "http://localhost:8080/";

// Biến hằng lưu giá trị các trường chưa được chọn
const gNONE_SELECTED = "";

// Mảng chứa dữ liệu phòng ban
var gPhongBanDb = {
    phongBan: [],
    findPhongBanObjByMaPhongBan: function (paramMaPhongBan) {
        var vFoundPhongBanObj = {};
        var vFoundPhongBanStatus = false;
        for (let bIndex = 0; !vFoundPhongBanStatus && bIndex < this.phongBan.length; bIndex++) {
            const element = this.phongBan[bIndex];
            if (paramMaPhongBan == element.maPhongBan) {
                vFoundPhongBanObj = JSON.parse(JSON.stringify(element));
                vFoundPhongBanStatus = true;
            }
        }
        return vFoundPhongBanObj;
    }
};

// Biến lưu trường thông tin cần focus nếu validate chưa chính xác
var gFocusField = "";

// Biến lưu thông tin đối tượng phòng ban
var gPhongBanObj = {
    id: -1,
    maPhongBan: "",
    kichCo: "",
    tenPhongBan: "",
    nghiepVu: "",
    gioiThieu: "",
    danhSachNhanVien: []
};

// Khai báo biến hằng lưu trữ trạng thái form
const gFORM_MODE_NORMAL = 'Normal';
const gFORM_MODE_INSERT = 'Insert';
const gFORM_MODE_UPDATE = 'Update';
const gFORM_MODE_DELETE = 'Delete';

// Biến toàn cục lưu trạng thái form, mặc định NORMAL
var gFormMode = gFORM_MODE_NORMAL;

const gDATA_COL = [
    "id",
    "maPhongBan",
    "tenPhongBan",
    "danhSachNhanVien",
    "action"
];
const gID_PHONG_BAN_COL = 0;
const gMA_PHONG_BAN_COL = 1;
const gTEN_PHONG_BAN_COL = 2;
const gDANH_SACH_NHAN_VIEN_COL = 4;
const gACTION_COL = 5;

// khởi tạo DataTable
var gTablePhongBan = $("#table-phong-ban").DataTable({
    "language": {
        "emptyTable": "Không có dữ liệu trong bảng",
        "info": "Hiển thị bản ghi _START_ tới _END_ trong tổng cộng _TOTAL_ bản ghi",
        "infoEmpty": "Hiển thị bản ghi 0 tới 0 trong tổng cộng 0 bản ghi",
        "infoFiltered": "(lọc từ tổng cộng _MAX_ bản ghi)",
        "infoPostFix": "",
        "thousands": ".",
        "lengthMenu": "Hiện _MENU_ bản ghi trên mỗi trang",
        "loadingRecords": "Loading...",
        "processing": "Processing...",
        "search": "Tìm kiếm:",
        "zeroRecords": "Không có bản ghi nào được tìm thấy",
        "paginate": {
            "first": "First",
            "last": "Last",
            "next": "Tiếp",
            "previous": "Trước"
        }
    },
    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
    "ordering": false,
    // "scrollY": "245px",
    // "scrollCollapse": true,
    // "scrollX": true,
    columns: [
        {
            data: gDATA_COL[gID_PHONG_BAN_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gMA_PHONG_BAN_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gTEN_PHONG_BAN_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gDANH_SACH_NHAN_VIEN_COL],
            className: "text-center",
            render: function () {
                return `<button class="btn btn-info btn-detail-phong-ban-nhan-vien" type="button">Danh sách nhân viên phòng ban</button>`;
            }
        },
        {
            data: gDATA_COL[gACTION_COL],
            className: "text-center",
            defaultContent: `
            <i class="far fa-edit fa-lg text-primary cursor-pointer icon-edit-phong-ban" data-toggle="tooltip" title="Sửa thông tin phòng ban"></i>
            <i class="far fa-trash-alt fa-lg text-danger cursor-pointer icon-delete-phong-ban" data-toggle="tooltip" title="Xoá phòng ban"></i>
            `
        }
    ]
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
// thực hiện hiển thị AdminLTE cho Select2 và Date
$(document).ready(function () {
    //Initialize Select2 Elements
    $('.select2').select2({
        theme: 'bootstrap4'
    });
    //Datemask dd/mm/yyyy
    $('[data-mask]').inputmask();
    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'DD/MM/YYYY'
    });
});

// 1 - R: Read 
// thực thi sự kiện tải trang
onPageLoading();

// 2 - C: Create
// thực thi sự kiện khi ấn nút Tạo phòng ban
$("#btn-create-phong-ban").on({
    click: function () {
        onBtnCreatePhongBanClick();
    }
});

// thực thi sự kiện khi ấn nút Create trên modal Tạo phòng ban
$("#modal-phong-ban-btn-create").on({
    click: function () {
        onModalPhongBanBtnCreateClick();
    }
});

// thực thi sự kiện khi ấn nút Create trên modal Tạo phòng ban
$("#table-phong-ban").on({
    click: function () {
        onBtnDetailPhongBanNhanVienClick(this);
    }
}, ".btn-detail-phong-ban-nhan-vien");

// 3 - U: Update
// thực thi sự kiện khi ấn icon Sửa thông tin phòng ban
$("#table-phong-ban").on({
    click: function () {
        onIconEditPhongBanClick(this);
    }
}, ".icon-edit-phong-ban");

// thực thi sự kiện khi ấn nút Update thông tin phòng ban trên modal
$("#modal-phong-ban-btn-update").on({
    click: function () {
        onModalPhongBanBtnUpdateClick();
    }
});

// 4 - D: Delete
// thực thi sự kiện khi ấn icon Sửa thông tin phòng ban
$("#table-phong-ban").on({
    click: function () {
        onIconDeletePhongBanClick(this);
    }
}, ".icon-delete-phong-ban");

// thực thi sự kiện khi ấn nút Confirm Delete phòng ban trên modal
$("#modal-btn-confirm-delete").on({
    click: function () {
        onModalBtnConfirmDeleteClick();
    }
});

// thực thi sự kiện khi đóng modal warning
$("#modal-warning").on({
    "hidden.bs.modal": function () {
        onModalWarningClose();
    }
});

// thực thi sự kiện khi đóng modal success
$("#modal-success").on({
    "hidden.bs.modal": function () {
        onModalSuccessClose();
    }
});

// thực thi sự kiện khi đóng modal phòng ban
$("#modal-phong-ban").on({
    "hidden.bs.modal": function () {
        onModalPhongBanClose();
    }
});

// thực thi sự kiện khi đóng modal danger
$("#modal-danger").on({
    "hidden.bs.modal": function () {
        onModalDangerClose();
    }
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// xử lý sự kiện khi tải trang
function onPageLoading() {
    gFormMode = gFORM_MODE_NORMAL;
    $("#form-mode").text(gFormMode);
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: gọi server và xử lý hiển thị
    callAjaxApiGetAllPhongBan();
}

// xử lý sự kiện khi ấn nút Tạo phòng ban
function onBtnCreatePhongBanClick() {
    gFormMode = gFORM_MODE_INSERT;
    $("#form-mode").text(gFormMode);
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: xử lý hiển thị
    loadModalPhongBan();
}

// xử lý sự kiện khi ấn nút Create phòng ban trên modal
function onModalPhongBanBtnCreateClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    console.log(gPhongBanObj);
    getPhongBanInfoObj(gPhongBanObj);
    console.log(gPhongBanObj);
    // B2: validate
    var vValidateStatus = validatePhongBanInfo(gPhongBanObj);
    // B3: gọi server và xử lý hiển thị
    if (vValidateStatus) {
        callAjaxApiCreatePhongBan(gPhongBanObj);
    }
}

// xử lý sự kiện khi đóng modal warning
function onModalWarningClose() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: xử lý hiển thị
    if (gFocusField.length !== 0) {
        getFocusField();
    }
}

// xử lý sự kiện khi ấn nút Chi tiết nhân viên
function onBtnDetailPhongBanNhanVienClick(paramBtnDetail) {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    getPhongBanObjByRow(paramBtnDetail, gPhongBanObj);
    // B2: validate
    // B3: gọi server và xử lý hiển thị
    var vUrlToOpen = "nhanvienCRUDTask74.html?phongbanid=" + gPhongBanObj.id;
    location.href = vUrlToOpen;
}

// xử lý sự kiện khi ấn icon Sửa thông tin phòng ban
function onIconEditPhongBanClick(paramIconEdit) {
    gFormMode = gFORM_MODE_UPDATE;
    $("#form-mode").text(gFormMode);
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    console.log(gPhongBanObj);
    getPhongBanObjByRow(paramIconEdit, gPhongBanObj);
    console.log(gPhongBanObj);
    // B2: validate
    // B3: gọi server và xử lý hiển thị
    loadModalPhongBan();
    callAjaxApiGetPhongBanByPhongBanId(gPhongBanObj);
}

// xử lý sự kiện khi ấn nút Update thông tin phòng ban trên modal
function onModalPhongBanBtnUpdateClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    getPhongBanInfoObj(gPhongBanObj);
    console.log(gPhongBanObj);
    // B2: validate
    var vValidateStatus = validatePhongBanInfo(gPhongBanObj);
    // B3: gọi server và xử lý hiển thị
    if (vValidateStatus) {
        callAjaxApiUpdatePhongBan(gPhongBanObj);
    }
}

// xử lý sự kiện khi ấn icon Xoá phòng ban
function onIconDeletePhongBanClick(paramIconDelete) {
    gFormMode = gFORM_MODE_DELETE;
    $("#form-mode").text(gFormMode);
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    console.log(gPhongBanObj);
    getPhongBanObjByRow(paramIconDelete, gPhongBanObj);
    console.log(gPhongBanObj);
    // B2: validate
    // B3: gọi server và xử lý hiển thị
    $("#modal-p-delete").html(`
        Xác nhận xoá phòng ban
        <span class="font-weight-bold">`+
        gPhongBanObj.tenPhongBan + ` (` + gPhongBanObj.maPhongBan + `)
        </span>
        có ID: <span class="font-weight-bold">` +
        gPhongBanObj.id +
        `</span> !!!`);
    $("#modal-danger").modal();
}

// xử lý sự kiện khi ấn nút Confirm Delete phòng ban trên modal
function onModalBtnConfirmDeleteClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    console.log(gPhongBanObj);
    // B2: validate
    // B3: gọi server và xử lý hiển thị
    callAjaxApiDeletePhongBan(gPhongBanObj);
}

// xử lý sự kiện khi đóng modal success
function onModalSuccessClose() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: xử lý hiển thị
    onPageLoading();
    $("#modal-phong-ban").modal("hide");
    $("#modal-danger").modal("hide");
}

// xử lý sự kiện khi đóng modal phòng ban
function onModalPhongBanClose() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: xử lý hiển thị
    resetToStart();
}

// xử lý sự kiện khi đóng modal danger
function onModalDangerClose() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: xử lý hiển thị
    resetToStart();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// gọi API lấy thông tin tất cả phòng ban
function callAjaxApiGetAllPhongBan() {
    $.ajax({
        url: gBASE_URL + "phongban",
        type: "GET",
        success: function (resAllPhongBanObj) {
            gPhongBanDb.phongBan = resAllPhongBanObj;
            loadAllPhongBanDataTable(gPhongBanDb.phongBan);
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// load thông tin tất cả phòng ban vào DataTable
function loadAllPhongBanDataTable(paramAllPhongBanObj) {
    gTablePhongBan.clear();
    gTablePhongBan.rows.add(paramAllPhongBanObj);
    gTablePhongBan.draw();
}

// load dữ liệu lựa chọn sẵn vào các trường trên modal phòng ban
function loadModalPhongBan() {
    if (gFormMode == gFORM_MODE_INSERT) {
        // tên title modal khi thêm mới phòng ban
        $("#modal-h4-title").text("tạo phòng ban mới");
        // ấn các trường thông tin không cần thiết khi tạo phòng ban
        $(".update-only").hide();
        // hiện nút Create và ẩn nút Update phòng ban
        $("#modal-phong-ban-btn-create").show();
        $("#modal-phong-ban-btn-update").hide();
    }
    if (gFormMode == gFORM_MODE_UPDATE) {
        // tên title modal khi cập nhật phòng ban
        $("#modal-h4-title").text("cập nhật thông tin phòng ban");
        // hiện đầy đủ các trường thông tin khi cập nhật thông tin phòng ban 
        $(".update-only").show();
        // hiện nút Update và ẩn nút Create phòng ban
        $("#modal-phong-ban-btn-update").show();
        $("#modal-phong-ban-btn-create").hide();
    }
    $("#modal-phong-ban").modal();
}

// lấy dữ liệu thông tin phòng ban được tạo
function getPhongBanInfoObj(paramPhongBanObj) {
    // chuẩn hoá
    $("#modal-phong-ban-inp-ma-phong-ban").val($("#modal-phong-ban-inp-ma-phong-ban").val().trim());
    $("#modal-phong-ban-inp-ten-phong-ban").val($("#modal-phong-ban-inp-ten-phong-ban").val().trim());
    $("#modal-phong-ban-textarea-nghiep-vu").val($("#modal-phong-ban-textarea-nghiep-vu").val().trim());
    $("#modal-phong-ban-textarea-gioi-thieu").val($("#modal-phong-ban-textarea-gioi-thieu").val().trim());
    // lưu thông tin 
    paramPhongBanObj.maPhongBan = $("#modal-phong-ban-inp-ma-phong-ban").val();
    paramPhongBanObj.tenPhongBan = $("#modal-phong-ban-inp-ten-phong-ban").val();
    paramPhongBanObj.nghiepVu = $("#modal-phong-ban-textarea-nghiep-vu").val();
    paramPhongBanObj.gioiThieu = $("#modal-phong-ban-textarea-gioi-thieu").val();
}

// validate thông tin phòng ban được tạo
function validatePhongBanInfo(paramPhongBanObj) {
    var vModalPWarningSelector = $("#modal-p-warning");
    var vModalWarningSelector = $("#modal-warning");
    if (paramPhongBanObj.maPhongBan.length == 0) {
        vModalPWarningSelector.text("Hãy điền Mã phòng ban !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-phong-ban-inp-ma-phong-ban");
        return false;
    }
    if (validateExsistedMaPhongBan(paramPhongBanObj)) {
        vModalPWarningSelector.text("Mã phòng ban đã tồn tại !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-phong-ban-inp-ma-phong-ban");
        return false;
    }
    if (paramPhongBanObj.tenPhongBan.length == 0) {
        vModalPWarningSelector.text("Hãy điền Tên phòng ban !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-phong-ban-inp-ten-phong-ban");
        return false;
    }
    if (paramPhongBanObj.nghiepVu.length == 0) {
        vModalPWarningSelector.text("Hãy điền Nghiệp vụ phòng ban !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-phong-ban-textarea-nghiep-vu");
        return false;
    }
    return true;
}

// validate mã phòng ban đã tồn tại
function validateExsistedMaPhongBan(paramPhongBanObj) {
    if (jQuery.isEmptyObject(gPhongBanDb.findPhongBanObjByMaPhongBan(paramPhongBanObj.maPhongBan))) {
        return false;
    }
    if (gFormMode == gFORM_MODE_UPDATE && paramPhongBanObj.id == gPhongBanDb.findPhongBanObjByMaPhongBan(paramPhongBanObj.maPhongBan).id) {
        return false;
    }
    return true;
}

// gọi API tạo phòng ban
function callAjaxApiCreatePhongBan(paramPhongBanObj) {
    $.ajax({
        url: gBASE_URL + "phongban",
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramPhongBanObj),
        success: function (resPhongBanObj) {
            console.log(paramPhongBanObj);
            console.log(resPhongBanObj); s
            $("#modal-p-success").html("Thông tin phòng ban được tạo thành công !!!");
            $("#modal-success").modal();
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// trả về id của đối tượng phòng ban muốn edit
function getPhongBanObjByRow(paramElement, paramPhongBanObj) {
    var vPhongBanRow = $(paramElement).parents("tr");
    var vDataPhongBanRow = gTablePhongBan.row(vPhongBanRow);
    var vPhongBanObjByDataPhongBanRow = vDataPhongBanRow.data();
    console.log(vPhongBanObjByDataPhongBanRow);
    paramPhongBanObj.id = vPhongBanObjByDataPhongBanRow.id;
    paramPhongBanObj.maPhongBan = vPhongBanObjByDataPhongBanRow.maPhongBan;
    paramPhongBanObj.tenPhongBan = vPhongBanObjByDataPhongBanRow.tenPhongBan;
    paramPhongBanObj.nghiepVu = vPhongBanObjByDataPhongBanRow.nghiepVu;
    paramPhongBanObj.gioiThieu = vPhongBanObjByDataPhongBanRow.gioiThieu;
    paramPhongBanObj.danhSachNhanVien = vPhongBanObjByDataPhongBanRow.danhSachNhanVien;
}

// gọi API lấy thông tin phòng ban theo phòng ban id
function callAjaxApiGetPhongBanByPhongBanId(paramPhongBanObj) {
    $.ajax({
        url: gBASE_URL + "phongban/" + paramPhongBanObj.id,
        type: "GET",
        success: function (resPhongBanObj) {
            gPhongBanObj = resPhongBanObj;
            console.log(gPhongBanObj);
            loadPhongBanObjToModalPhongBan(gPhongBanObj);
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// load thông tin đối tượng phòng ban vào modal phòng ban
function loadPhongBanObjToModalPhongBan(paramPhongBanObj) {
    $("#modal-phong-ban-inp-id").val(paramPhongBanObj.id);
    $("#modal-phong-ban-inp-ma-phong-ban").val(paramPhongBanObj.maPhongBan);
    $("#modal-phong-ban-inp-ten-phong-ban").val(paramPhongBanObj.tenPhongBan);
    $("#modal-phong-ban-textarea-nghiep-vu").val(paramPhongBanObj.nghiepVu);
    $("#modal-phong-ban-textarea-gioi-thieu").val(paramPhongBanObj.gioiThieu);
}

// gọi API cập nhât thông tin phòng ban
function callAjaxApiUpdatePhongBan(paramPhongBanObj) {
    $.ajax({
        url: gBASE_URL + "phongban/" + paramPhongBanObj.id,
        type: "PUT",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramPhongBanObj),
        success: function (resPhongBanObj) {
            console.log(gPhongBanObj);
            console.log(resPhongBanObj);
            $("#modal-p-success").html("Cập nhật thông tin phòng ban thành công !!!");
            $("#modal-success").modal();
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// gọi API xoá thông tin phòng ban
function callAjaxApiDeletePhongBan(paramPhongBanObj) {
    $.ajax({
        url: gBASE_URL + "phongban/" + paramPhongBanObj.id,
        type: "DELETE",
        success: function (resPhongBanObj) {
            console.log(gPhongBanObj);
            console.log(resPhongBanObj);
            $("#modal-p-success").html("Xoá thông tin phòng ban thành công !!!");
            $("#modal-success").modal();
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// focus vào trường thông tin trên modal chưa thoả mãn validate
function getFocusField() {
    gFocusField.focus();
}

// reset dữ liệu các dữ liệu biến global và thông tin trên modal user detail
function resetToStart() {
    gFocusField = "";
    gPhongBanObj = {
        id: -1,
        maPhongBan: "",
        kichCo: "",
        tenPhongBan: "",
        nghiepVu: "",
        gioiThieu: "",
        danhSachNhanVien: []
    };
    gFormMode = gFORM_MODE_NORMAL;
    $("#form-mode").text(gFormMode);
    $("#modal-phong-ban input,textarea").val("");
    console.log(gPhongBanObj);
}