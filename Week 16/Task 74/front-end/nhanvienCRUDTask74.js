"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// URL get API
const gBASE_URL = "http://localhost:8080/";

// Biến hằng lưu giá trị các trường chưa được chọn
const gNONE_SELECTED = "";

// Mảng chứa dữ liệu nhân viên
var gNhanVienDb = {
    nhanVien: [],
    findNhanVienObjByMaNhanVien: function (paramMaNhanVien) {
        var vFoundNhanVienObj = {};
        var vFoundNhanVienStatus = false;
        for (let bIndex = 0; !vFoundNhanVienStatus && bIndex < this.nhanVien.length; bIndex++) {
            const element = this.nhanVien[bIndex];
            if (paramMaNhanVien == element.maNhanVien) {
                vFoundNhanVienObj = JSON.parse(JSON.stringify(element));
                vFoundNhanVienStatus = true;
            }
        }
        return vFoundNhanVienObj;
    },
    findNhanVienObjBySoDienThoai: function (paramSoDienThoai) {
        var vFoundNhanVienObj = {};
        var vFoundNhanVienStatus = false;
        for (let bIndex = 0; !vFoundNhanVienStatus && bIndex < this.nhanVien.length; bIndex++) {
            const element = this.nhanVien[bIndex];
            if (paramSoDienThoai == element.soDienThoai) {
                vFoundNhanVienObj = JSON.parse(JSON.stringify(element));
                vFoundNhanVienStatus = true;
            }
        }
        return vFoundNhanVienObj;
    }
};

// Mảng chứa dữ liệu phòng ban
var gPhongBanDb = {
    phongBan: [],
    findPhongBanObjByMaPhongBan: function (paramMaPhongBan) {
        var vFoundPhongBanObj = {};
        var vFoundPhongBanStatus = false;
        for (let bIndex = 0; !vFoundPhongBanStatus && bIndex < this.phongBan.length; bIndex++) {
            const element = this.phongBan[bIndex];
            if (paramMaPhongBan == element.maPhongBan) {
                vFoundPhongBanObj = JSON.parse(JSON.stringify(element));
                vFoundPhongBanStatus = true;
            }
        }
        return vFoundPhongBanObj;
    }
};

// Biến lưu trường thông tin cần focus nếu validate chưa chính xác
var gFocusField = "";

// Biến lưu thông tin đối tượng nhân viên
var gNhanVienObj = {
    id: -1,
    maNhanVien: "",
    tenNhanVien: "",
    chucVu: "",
    gioiTinh: "",
    ngaySinh: "",
    diaChi: "",
    soDienThoai: "",
    phongBanId: -1
};

// Biến lưu thông tin đối tượng phòng ban
var gPhongBanObj = {
    id: -1,
    maPhongBan: "",
    kichCo: "",
    tenPhongBan: "",
    nghiepVu: "",
    gioiThieu: "",
    danhSachNhanVien: []
};

// Khai báo biến hằng lưu trữ trạng thái form
const gFORM_MODE_NORMAL = 'Normal';
const gFORM_MODE_INSERT = 'Insert';
const gFORM_MODE_UPDATE = 'Update';
const gFORM_MODE_DELETE = 'Delete';

// Biến toàn cục lưu trạng thái form, mặc định NORMAL
var gFormMode = gFORM_MODE_NORMAL;

const gDATA_COL = [
    "id",
    "maNhanVien",
    "tenNhanVien",
    "chucVu",
    "gioiTinh",
    "ngaySinh",
    "diaChi",
    "soDienThoai",
    "action"
];
const gID_NHAN_VIEN_COL = 0;
const gMA_NHAN_VIEN_COL = 1;
const gTEN_NHAN_VIEN_COL = 2;
const gCHUC_VU_COL = 3;
const gGIOI_TINH_COL = 4;
const gNGAY_SINH_COL = 5;
const gDIA_CHI_COL = 6;
const gSO_DIEN_THOAI_COL = 7;
const gACTION_COL = 8;

// khởi tạo DataTable
var gTableNhanVien = $("#table-nhan-vien").DataTable({
    "language": {
        "emptyTable": "Không có dữ liệu trong bảng",
        "info": "Hiển thị bản ghi _START_ tới _END_ trong tổng cộng _TOTAL_ bản ghi",
        "infoEmpty": "Hiển thị bản ghi 0 tới 0 trong tổng cộng 0 bản ghi",
        "infoFiltered": "(lọc từ tổng cộng _MAX_ bản ghi)",
        "infoPostFix": "",
        "thousands": ".",
        "lengthMenu": "Hiện _MENU_ bản ghi trên mỗi trang",
        "loadingRecords": "Loading...",
        "processing": "Processing...",
        "search": "Tìm kiếm:",
        "zeroRecords": "Không có bản ghi nào được tìm thấy",
        "paginate": {
            "first": "First",
            "last": "Last",
            "next": "Tiếp",
            "previous": "Trước"
        }
    },
    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
    "ordering": false,
    // "scrollY": "245px",
    // "scrollCollapse": true,
    // "scrollX": true,
    columns: [
        {
            data: gDATA_COL[gID_NHAN_VIEN_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gMA_NHAN_VIEN_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gTEN_NHAN_VIEN_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gCHUC_VU_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gGIOI_TINH_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gNGAY_SINH_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gDIA_CHI_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gSO_DIEN_THOAI_COL],
            className: "text-center"
        },
        {
            data: gDATA_COL[gACTION_COL],
            className: "text-center",
            defaultContent: `
            <i class="far fa-edit fa-lg text-primary cursor-pointer icon-edit-nhan-vien" data-toggle="tooltip" title="Sửa thông tin nhân viên"></i>
            <i class="far fa-trash-alt fa-lg text-danger cursor-pointer icon-delete-nhan-vien" data-toggle="tooltip" title="Xoá nhân viên"></i>
            `
        }
    ]
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
// thực hiện hiển thị AdminLTE cho Select2 và Date
$(document).ready(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })

    //Money Euro (active Datemask dd/mm/yyyy)
    $('[data-mask]').inputmask()

    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'DD/MM/YYYY'
    });
});

// 1 - R: Read 
// thực thi sự kiện tải trang
onPageLoading();

// 2 - C: Create
// thực thi sự kiện khi ấn nút Tạo nhân viên
$("#btn-create-nhan-vien").on({
    click: function () {
        onBtnCreateNhanVienClick();
    }
});

// thực thi sự kiện khi ấn nút Create trên modal Tạo nhân viên
$("#modal-nhan-vien-btn-create").on({
    click: function () {
        onModalNhanVienBtnCreateClick();
    }
});

// 3 - U: Update
// thực thi sự kiện khi ấn icon Sửa thông tin nhân viên
$("#table-nhan-vien").on({
    click: function () {
        onIconEditNhanVienClick(this);
    }
}, ".icon-edit-nhan-vien");

// thực thi sự kiện khi ấn nút Update thông tin nhân viên trên modal
$("#modal-nhan-vien-btn-update").on({
    click: function () {
        onModalNhanVienBtnUpdateClick();
    }
});

// 4 - D: Delete
// thực thi sự kiện khi ấn icon Sửa thông tin nhân viên
$("#table-nhan-vien").on({
    click: function () {
        onIconDeleteNhanVienClick(this);
    }
}, ".icon-delete-nhan-vien");

// thực thi sự kiện khi ấn nút Confirm Delete nhân viên trên modal
$("#modal-btn-confirm-delete").on({
    click: function () {
        onModalBtnConfirmDeleteClick();
    }
});

// thực thi sự kiện khi đóng modal warning
$("#modal-warning").on({
    "hidden.bs.modal": function () {
        onModalWarningClose();
    }
});

// thực thi sự kiện khi đóng modal success
$("#modal-success").on({
    "hidden.bs.modal": function () {
        onModalSuccessClose();
    }
});

// thực thi sự kiện khi đóng modal nhân viên
$("#modal-nhan-vien").on({
    "hidden.bs.modal": function () {
        onModalNhanVienClose();
    }
});

// thực thi sự kiện khi đóng modal danger
$("#modal-danger").on({
    "hidden.bs.modal": function () {
        onModalDangerClose();
    }
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// xử lý sự kiện khi tải trang
function onPageLoading() {
    gFormMode = gFORM_MODE_NORMAL;
    $("#form-mode").text(gFormMode);
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    var vUrl = location.href;
    var vUrlObj = new URL(vUrl);
    var vPhongBanId = vUrlObj.searchParams.get("phongbanid");
    // B2: validate
    // B3: gọi server và xử lý hiển thị
    var vApiUrl = gBASE_URL + "phongban/";
    $("#h3-title-table-nhan-vien").text("danh sách tất cả nhân viên");
    if (vPhongBanId != null) {
        vApiUrl = vApiUrl + vPhongBanId + "/";
        callAjaxApiGetPhongBanByPhongBanId(vPhongBanId);
    }
    vApiUrl = vApiUrl + "nhanvien";
    callAjaxApiGetAllNhanVien(vApiUrl);
}

// xử lý sự kiện khi ấn nút Tạo nhân viên
function onBtnCreateNhanVienClick() {
    gFormMode = gFORM_MODE_INSERT;
    $("#form-mode").text(gFormMode);
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: gọi server 
    callAjaxApiGetAllPhongBan();
    // B4: xử lý hiển thị
    loadModalNhanVien();
}

// xử lý sự kiện khi ấn nút Create nhân viên trên modal
function onModalNhanVienBtnCreateClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    console.log(gNhanVienObj);
    getNhanVienInfoObj(gNhanVienObj);
    console.log(gNhanVienObj);
    // B2: validate
    var vValidateStatus = validateNhanVienInfo(gNhanVienObj);
    // B3: gọi server và xử lý hiển thị
    if (vValidateStatus) {
        callAjaxApiCreateNhanVien(gNhanVienObj);
    }
}

// xử lý sự kiện khi đóng modal warning
function onModalWarningClose() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: xử lý hiển thị
    if (gFocusField.length !== 0) {
        getFocusField();
    }
}

// xử lý sự kiện khi ấn icon Sửa thông tin nhân viên
function onIconEditNhanVienClick(paramIconEdit) {
    gFormMode = gFORM_MODE_UPDATE;
    $("#form-mode").text(gFormMode);
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    console.log(gNhanVienObj);
    getNhanVienObjByRow(paramIconEdit, gNhanVienObj);
    console.log(gNhanVienObj);
    // B2: validate
    // B3: gọi server và xử lý hiển thị
    loadModalNhanVien();
    callAjaxApiGetNhanVienByNhanVienId(gNhanVienObj);
}

// xử lý sự kiện khi ấn nút Update thông tin nhân viên trên modal
function onModalNhanVienBtnUpdateClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    getNhanVienInfoObj(gNhanVienObj);
    console.log(gNhanVienObj);
    // B2: validate
    var vValidateStatus = validateNhanVienInfo(gNhanVienObj);
    // B3: gọi server và xử lý hiển thị
    if (vValidateStatus) {
        callAjaxApiUpdateNhanVien(gNhanVienObj);
    }
}

// xử lý sự kiện khi ấn icon Xoá nhân viên
function onIconDeleteNhanVienClick(paramIconDelete) {
    gFormMode = gFORM_MODE_DELETE;
    $("#form-mode").text(gFormMode);
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    console.log(gNhanVienObj);
    getNhanVienObjByRow(paramIconDelete, gNhanVienObj);
    console.log(gNhanVienObj);
    // B2: validate
    // B3: gọi server và xử lý hiển thị
    $("#modal-p-delete").html(`
        Xác nhận xoá nhân viên
        <span class="font-weight-bold">`+
        gNhanVienObj.tenNhanVien + ` (` + gNhanVienObj.maNhanVien + `)
        </span>
        có ID: <span class="font-weight-bold">` +
        gNhanVienObj.id +
        `</span> !!!`);
    $("#modal-danger").modal();
}

// xử lý sự kiện khi ấn nút Confirm Delete nhân viên trên modal
function onModalBtnConfirmDeleteClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    console.log(gNhanVienObj);
    // B2: validate
    // B3: gọi server và xử lý hiển thị
    callAjaxApiDeleteNhanVien(gNhanVienObj);
}

// xử lý sự kiện khi đóng modal success
function onModalSuccessClose() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: xử lý hiển thị
    onPageLoading();
    $("#modal-nhan-vien").modal("hide");
    $("#modal-danger").modal("hide");
}

// xử lý sự kiện khi đóng modal nhân viên
function onModalNhanVienClose() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: xử lý hiển thị
    resetToStart();
}

// xử lý sự kiện khi đóng modal danger
function onModalDangerClose() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate
    // B3: xử lý hiển thị
    resetToStart();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// gọi API lấy thông tin phòng ban theo phòng ban id
function callAjaxApiGetPhongBanByPhongBanId(paramPhongBanId) {
    $.ajax({
        url: gBASE_URL + "phongban/" + paramPhongBanId,
        type: "GET",
        success: function (resPhongBanObj) {
            gPhongBanObj = resPhongBanObj;
            console.log(gPhongBanObj);
            $("#h3-title-table-nhan-vien").html(`
                danh sách tất cả nhân viên
                <span class="text-danger">
                    phòng ban `+ gPhongBanObj.tenPhongBan +
                `</span>
                (` + gPhongBanObj.maPhongBan + ` - ID: ` + gPhongBanObj.id + `)`
            );
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// gọi API lấy thông tin tất cả nhân viên (hoặc danh sách nhân viên của phòng ban)
function callAjaxApiGetAllNhanVien(paramApiUrl) {
    $.ajax({
        url: paramApiUrl,
        type: "GET",
        success: function (resAllNhanVienObj) {
            gNhanVienDb.nhanVien = resAllNhanVienObj;
            loadAllNhanVienDataTable(gNhanVienDb.nhanVien);
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// load thông tin tất cả nhân viên vào DataTable
function loadAllNhanVienDataTable(paramNhanVienObj) {
    gTableNhanVien.clear();
    gTableNhanVien.rows.add(paramNhanVienObj);
    gTableNhanVien.draw();
}

// gọi API lấy thông tin tất cả phòng ban
function callAjaxApiGetAllPhongBan() {
    $.ajax({
        url: gBASE_URL + "phongban",
        type: "GET",
        success: function (resAllPhongBanObj) {
            gPhongBanDb.phongBan = resAllPhongBanObj;
            loadAllPhongBanObjToModalNhanVien(gPhongBanDb.phongBan);
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// load thông tin tất cả phòng ban vào modal select nhân viên
function loadAllPhongBanObjToModalNhanVien(paramAllPhongBanObj) {
    var vModalNhanVienSelectIdPhongBan = $("#modal-nhan-vien-select-id-phong-ban");
    vModalNhanVienSelectIdPhongBan
        .empty()
        .append($("<option/>", {
            value: gNONE_SELECTED,
            text: "--- Chọn phòng ban ---"
        }));
    for (let bIndex = 0; bIndex < paramAllPhongBanObj.length; bIndex++) {
        const element = paramAllPhongBanObj[bIndex];
        $("<option/>", {
            value: element.maPhongBan,
            text: element.tenPhongBan + ` (` + element.maPhongBan + ` - ID: ` + element.id + `)`
        }).appendTo(vModalNhanVienSelectIdPhongBan);
    }
}

// load dữ liệu lựa chọn sẵn vào các trường trên modal nhân viên
function loadModalNhanVien() {
    if (gFormMode == gFORM_MODE_INSERT) {
        // tên title modal khi thêm mới nhân viên
        $("#modal-h4-title").text("tạo nhân viên mới");
        // ấn các trường thông tin không cần thiết khi tạo nhân viên
        $(".create-only").show();
        $(".update-only").hide();
        // hiện nút Create và ẩn nút Update nhân viên
        $("#modal-nhan-vien-btn-create").show();
        $("#modal-nhan-vien-btn-update").hide();
    }
    if (gFormMode == gFORM_MODE_UPDATE) {
        // tên title modal khi cập nhật nhân viên
        $("#modal-h4-title").text("cập nhật thông tin nhân viên");
        // hiện đầy đủ các trường thông tin khi cập nhật thông tin nhân viên 
        $(".update-only").show();
        $(".create-only").hide();
        // hiện nút Update và ẩn nút Create nhân viên
        $("#modal-nhan-vien-btn-update").show();
        $("#modal-nhan-vien-btn-create").hide();
    }
    $("#modal-nhan-vien").modal();
}

// lấy dữ liệu thông tin nhân viên được tạo
function getNhanVienInfoObj(paramNhanVienObj) {
    // chuẩn hoá
    $("#modal-nhan-vien-inp-ma-nhan-vien").val($("#modal-nhan-vien-inp-ma-nhan-vien").val().trim());
    $("#modal-nhan-vien-inp-ten-nhan-vien").val($("#modal-nhan-vien-inp-ten-nhan-vien").val().trim());
    $("#modal-nhan-vien-inp-chuc-vu").val($("#modal-nhan-vien-inp-chuc-vu").val().trim());
    $("#modal-nhan-vien-inp-gioi-tinh").val($("#modal-nhan-vien-inp-gioi-tinh").val().trim());
    $("#modal-nhan-vien-inp-ngay-sinh").val($("#modal-nhan-vien-inp-ngay-sinh").val().trim());
    $("#modal-nhan-vien-inp-dia-chi").val($("#modal-nhan-vien-inp-dia-chi").val().trim());
    $("#modal-nhan-vien-inp-so-dien-thoai").val($("#modal-nhan-vien-inp-so-dien-thoai").val().trim());
    // lưu thông tin 
    paramNhanVienObj.maNhanVien = $("#modal-nhan-vien-inp-ma-nhan-vien").val();
    paramNhanVienObj.tenNhanVien = $("#modal-nhan-vien-inp-ten-nhan-vien").val();
    paramNhanVienObj.chucVu = $("#modal-nhan-vien-inp-chuc-vu").val();
    paramNhanVienObj.gioiTinh = $("#modal-nhan-vien-inp-gioi-tinh").val();
    paramNhanVienObj.ngaySinh = $("#modal-nhan-vien-inp-ngay-sinh").val();
    paramNhanVienObj.diaChi = $("#modal-nhan-vien-inp-dia-chi").val();
    paramNhanVienObj.soDienThoai = $("#modal-nhan-vien-inp-so-dien-thoai").val();
    paramNhanVienObj.phongBanId = gPhongBanDb.findPhongBanObjByMaPhongBan($("#modal-nhan-vien-select-id-phong-ban").val()).id;
    if ($("#modal-nhan-vien-select-id-phong-ban").val() == gNONE_SELECTED) {
        paramNhanVienObj.phongBanId = gNONE_SELECTED;
    }
}

// validate thông tin nhân viên được tạo
function validateNhanVienInfo(paramNhanVienObj) {
    var vModalPWarningSelector = $("#modal-p-warning");
    var vModalWarningSelector = $("#modal-warning");
    if (gFormMode == gFORM_MODE_INSERT && paramNhanVienObj.phongBanId == gNONE_SELECTED) {
        vModalPWarningSelector.text("Hãy chọn Phòng ban !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-select-id-phong-ban");
        return false;
    }
    if (paramNhanVienObj.maNhanVien.length == 0) {
        vModalPWarningSelector.text("Hãy điền Mã nhân viên !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-inp-ma-nhan-vien");
        return false;
    }
    if (validateExsistedMaNhanVien(paramNhanVienObj)) {
        vModalPWarningSelector.text("Mã nhân viên đã tồn tại!!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-inp-ma-nhan-vien");
        return false;
    }
    if (paramNhanVienObj.tenNhanVien.length == 0) {
        vModalPWarningSelector.text("Hãy điền Tên nhân viên !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-inp-ten-nhan-vien");
        return false;
    }
    if (paramNhanVienObj.chucVu.length == 0) {
        vModalPWarningSelector.text("Hãy điền Chức vụ nhân viên !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-inp-chuc-vu");
        return false;
    }
    if (paramNhanVienObj.gioiTinh.length == 0) {
        vModalPWarningSelector.text("Hãy điền Giới tính nhân viên !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-inp-gioi-tinh");
        return false;
    }
    if (!validateDateFormat(paramNhanVienObj.ngaySinh)) {
        vModalPWarningSelector.text("Hãy điền Ngày sinh nhân viên đúng định dạng dd/MM/yyyy !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-inp-ngay-sinh");
        return false;
    }
    if (paramNhanVienObj.diaChi.length == 0) {
        vModalPWarningSelector.text("Hãy điền Địa chỉ nhân viên !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-inp-dia-chi");
        return false;
    }
    if (paramNhanVienObj.soDienThoai.length == 0) {
        vModalPWarningSelector.text("Hãy điền Số điện thoại nhân viên !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-inp-so-dien-thoai");
        return false;
    }
    if (!validatePhoneNumberFormat(paramNhanVienObj.soDienThoai)) {
        vModalPWarningSelector.text("Hãy điền Số điện thoại đúng định dạng 10 số !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-inp-so-dien-thoai");
        return false;
    }
    if (validateExsistedSoDienThoai(paramNhanVienObj)) {
        vModalPWarningSelector.text("Số điện thoại đã tồn tại !!!");
        vModalWarningSelector.modal();
        gFocusField = $("#modal-nhan-vien-inp-so-dien-thoai");
        return false;
    }
    return true;
}

// validate mã nhân viên đã tồn tại
function validateExsistedMaNhanVien(paramNhanVienObj) {
    if (jQuery.isEmptyObject(gNhanVienDb.findNhanVienObjByMaNhanVien(paramNhanVienObj.maNhanVien))) {
        return false;
    }
    if (gFormMode == gFORM_MODE_UPDATE && paramNhanVienObj.id == gNhanVienDb.findNhanVienObjByMaNhanVien(paramNhanVienObj.maNhanVien).id) {
        return false;
    }
    return true;
}

// validate số điện thoại đã tồn tại
function validateExsistedSoDienThoai(paramNhanVienObj) {
    if (jQuery.isEmptyObject(gNhanVienDb.findNhanVienObjBySoDienThoai(paramNhanVienObj.soDienThoai))) {
        return false;
    }
    if (gFormMode == gFORM_MODE_UPDATE && paramNhanVienObj.id == gNhanVienDb.findNhanVienObjBySoDienThoai(paramNhanVienObj.soDienThoai).id) {
        return false;
    }
    return true;
}


// validate date format dd/MM/yyyy
function validateDateFormat(paramDate) {
    const vDateRE = /^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/\-]\d{4}$/;
    return vDateRE.test(String(paramDate));
}

// validate phone number format 10-digit
function validatePhoneNumberFormat(paramPhoneNumber) {
    const vPhoneNumberRE = /(^$|[0-9]{10})/;
    return vPhoneNumberRE.test(String(paramPhoneNumber));
}

// gọi API tạo nhân viên
function callAjaxApiCreateNhanVien(paramNhanVienObj) {
    $.ajax({
        url: gBASE_URL + "phongban/" + paramNhanVienObj.phongBanId + "/nhanvien",
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramNhanVienObj),
        success: function (resNhanVienObj) {
            console.log(paramNhanVienObj);
            console.log(resNhanVienObj);
            $("#modal-p-success").html("Thông tin nhân viên được tạo thành công !!!");
            $("#modal-success").modal();
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// trả về id của đối tượng nhân viên muốn edit
function getNhanVienObjByRow(paramIcon, paramNhanVienObj) {
    var vNhanVienRow = $(paramIcon).parents("tr");
    var vDataNhanVienRow = gTableNhanVien.row(vNhanVienRow);
    var vNhanVienObjByDataNhanVienRow = vDataNhanVienRow.data();
    console.log(vNhanVienObjByDataNhanVienRow);
    paramNhanVienObj.id = vNhanVienObjByDataNhanVienRow.id;
    paramNhanVienObj.maNhanVien = vNhanVienObjByDataNhanVienRow.maNhanVien;
    paramNhanVienObj.tenNhanVien = vNhanVienObjByDataNhanVienRow.tenNhanVien;
    paramNhanVienObj.chucVu = vNhanVienObjByDataNhanVienRow.chucVu;
    paramNhanVienObj.gioiTinh = vNhanVienObjByDataNhanVienRow.gioiTinh;
    paramNhanVienObj.ngaySinh = vNhanVienObjByDataNhanVienRow.ngaySinh;
    paramNhanVienObj.diaChi = vNhanVienObjByDataNhanVienRow.diaChi;
    paramNhanVienObj.soDienThoai = vNhanVienObjByDataNhanVienRow.soDienThoai;
}

// gọi API lấy thông tin nhân viên theo nhân viên id
function callAjaxApiGetNhanVienByNhanVienId(paramNhanVienObj) {
    $.ajax({
        url: gBASE_URL + "phongban/nhanvien/" + paramNhanVienObj.id,
        type: "GET",
        success: function (resNhanVienObj) {
            gNhanVienObj = resNhanVienObj;
            console.log(gNhanVienObj);
            loadNhanVienObjToModalNhanVien(gNhanVienObj);
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// load thông tin đối tượng nhân viên vào modal nhân viên
function loadNhanVienObjToModalNhanVien(paramNhanVienObj) {
    $("#modal-nhan-vien-inp-id").val(paramNhanVienObj.id);
    $("#modal-nhan-vien-inp-ma-nhan-vien").val(paramNhanVienObj.maNhanVien);
    $("#modal-nhan-vien-inp-ten-nhan-vien").val(paramNhanVienObj.tenNhanVien);
    $("#modal-nhan-vien-inp-chuc-vu").val(paramNhanVienObj.chucVu);
    $("#modal-nhan-vien-inp-gioi-tinh").val(paramNhanVienObj.gioiTinh);
    $("#modal-nhan-vien-inp-ngay-sinh").val(paramNhanVienObj.ngaySinh);
    $("#modal-nhan-vien-inp-dia-chi").val(paramNhanVienObj.diaChi);
    $("#modal-nhan-vien-inp-so-dien-thoai").val(paramNhanVienObj.soDienThoai);
}

// gọi API cập nhât thông tin nhân viên
function callAjaxApiUpdateNhanVien(paramNhanVienObj) {
    $.ajax({
        url: gBASE_URL + "phongban/nhanvien/" + paramNhanVienObj.id,
        type: "PUT",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramNhanVienObj),
        success: function (resNhanVienObj) {
            console.log(gNhanVienObj);
            console.log(resNhanVienObj);
            $("#modal-p-success").html("Cập nhật thông tin nhân viên thành công !!!");
            $("#modal-success").modal();
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// gọi API xoá thông tin nhân viên
function callAjaxApiDeleteNhanVien(paramNhanVienObj) {
    $.ajax({
        url: gBASE_URL + "phongban/nhanvien/" + paramNhanVienObj.id,
        type: "DELETE",
        success: function (resNhanVienObj) {
            console.log(gNhanVienObj);
            console.log(resNhanVienObj);
            $("#modal-p-success").html("Xoá thông tin nhân viên thành công !!!");
            $("#modal-success").modal();
        },
        error: function (err) {
            console.log(err);
            if (err.responseJSON !== undefined) {
                alert(err.responseJSON.errors + " (" + err.status + ")");
            } else {
                alert(err.responseText + " (" + err.status + ")");
            }
        }
    });
}

// focus vào trường thông tin trên modal chưa thoả mãn validate
function getFocusField() {
    gFocusField.focus();
}

// reset dữ liệu các dữ liệu biến global và thông tin trên modal user detail
function resetToStart() {
    gFocusField = "";
    gNhanVienObj = {
        id: -1,
        maNhanVien: "",
        tenNhanVien: "",
        chucVu: "",
        gioiTinh: "",
        ngaySinh: "",
        diaChi: "",
        soDienThoai: "",
        phongBanId: -1
    };
    gFormMode = gFORM_MODE_NORMAL;
    $("#form-mode").text(gFormMode);
    $("#modal-nhan-vien input,textarea").val("");
    console.log(gNhanVienObj);
}