package com.devcamp.company.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.company.model.CNhanVien;
import com.devcamp.company.repository.INhanVienRepository;
import com.devcamp.company.repository.IPhongBanRepository;

@RestController
@CrossOrigin
public class CNhanVienController {
	@Autowired
	IPhongBanRepository phongBanRepository;

	@Autowired
	INhanVienRepository nhanVienRepository;

	@GetMapping(value = { "/phongban/nhanvien", "/phongban/{phongbanid}/nhanvien" })
	public ResponseEntity<Object> getAllNhanVien(@PathVariable(required = false) Long phongbanid) {
		try {
			if (phongbanid == null) {
				return new ResponseEntity<>(nhanVienRepository.findAll(), HttpStatus.OK);
			}

			if (!phongBanRepository.existsById(phongbanid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<>(nhanVienRepository.findByPhongBanId(phongbanid), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@GetMapping(value = { "/phongban/nhanvien/{nhanvienid}", "/phongban/{phongbanid}/nhanvien/{nhanvienid}" })
	public ResponseEntity<Object> getNhanVienById(@PathVariable(required = false) Long phongbanid,
			@PathVariable Long nhanvienid) {
		try {
			if (!nhanVienRepository.existsById(nhanvienid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			if (phongbanid == null) {
				return new ResponseEntity<>(nhanVienRepository.findById(nhanvienid), HttpStatus.OK);
			}

			if (!phongBanRepository.existsById(phongbanid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			CNhanVien foundNhanVien = nhanVienRepository.findByIdAndPhongBanId(nhanvienid, phongbanid);

			if (foundNhanVien == null) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<>(foundNhanVien, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@PostMapping("/phongban/{phongbanid}/nhanvien")
	public ResponseEntity<Object> createNhanVien(@PathVariable Long phongbanid,
			@Valid @RequestBody CNhanVien cNhanVien) {
		try {
			if (!phongBanRepository.existsById(phongbanid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			CNhanVien newNhanVien = new CNhanVien();
			newNhanVien.setMaNhanVien(cNhanVien.getMaNhanVien().trim());
			newNhanVien.setTenNhanVien(cNhanVien.getTenNhanVien().trim());
			newNhanVien.setChucVu(cNhanVien.getChucVu().trim());
			newNhanVien.setGioiTinh(cNhanVien.getGioiTinh().trim());
			newNhanVien.setNgaySinh(cNhanVien.getNgaySinh());
			newNhanVien.setDiaChi(cNhanVien.getDiaChi().trim());
			newNhanVien.setSoDienThoai(cNhanVien.getSoDienThoai());
			newNhanVien.setPhongBan(phongBanRepository.findById(phongbanid).get());

			return new ResponseEntity<>(nhanVienRepository.save(newNhanVien), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping(value = { "/phongban/nhanvien/{nhanvienid}", "/phongban/{phongbanid}/nhanvien/{nhanvienid}" })
	public ResponseEntity<Object> updateNhanVien(@PathVariable(required = false) Long phongbanid,
			@PathVariable Long nhanvienid, @Valid @RequestBody CNhanVien cNhanVien) {
		try {
			if (!nhanVienRepository.existsById(nhanvienid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			CNhanVien updateNhanVien = nhanVienRepository.findById(nhanvienid).get();

			if (phongbanid == null) {

				updateNhanVien.setMaNhanVien(cNhanVien.getMaNhanVien().trim());
				updateNhanVien.setTenNhanVien(cNhanVien.getTenNhanVien().trim());
				updateNhanVien.setChucVu(cNhanVien.getChucVu().trim());
				updateNhanVien.setGioiTinh(cNhanVien.getGioiTinh().trim());
				updateNhanVien.setNgaySinh(cNhanVien.getNgaySinh());
				updateNhanVien.setDiaChi(cNhanVien.getDiaChi().trim());
				updateNhanVien.setSoDienThoai(cNhanVien.getSoDienThoai());

				return new ResponseEntity<>(nhanVienRepository.save(updateNhanVien), HttpStatus.OK);
			}

			if (!phongBanRepository.existsById(phongbanid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			updateNhanVien = nhanVienRepository.findByIdAndPhongBanId(nhanvienid, phongbanid);

			if (updateNhanVien == null) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			updateNhanVien.setMaNhanVien(cNhanVien.getMaNhanVien().trim());
			updateNhanVien.setTenNhanVien(cNhanVien.getTenNhanVien().trim());
			updateNhanVien.setChucVu(cNhanVien.getChucVu().trim());
			updateNhanVien.setGioiTinh(cNhanVien.getGioiTinh().trim());
			updateNhanVien.setNgaySinh(cNhanVien.getNgaySinh());
			updateNhanVien.setDiaChi(cNhanVien.getDiaChi().trim());
			updateNhanVien.setSoDienThoai(cNhanVien.getSoDienThoai());

			return new ResponseEntity<>(nhanVienRepository.save(updateNhanVien), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping(value = { "/phongban/nhanvien/{nhanvienid}", "/phongban/{phongbanid}/nhanvien/{nhanvienid}" })
	public ResponseEntity<Object> deleteNhanVienById(@PathVariable(required = false) Long phongbanid,
			@PathVariable Long nhanvienid) {
		try {
			if (!nhanVienRepository.existsById(nhanvienid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			if (phongbanid == null) {

				nhanVienRepository.deleteById(nhanvienid);

				return new ResponseEntity<>(null, HttpStatus.OK);
			}

			if (!phongBanRepository.existsById(phongbanid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			CNhanVien deleteNhanVien = nhanVienRepository.findByIdAndPhongBanId(nhanvienid, phongbanid);

			if (deleteNhanVien == null) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			nhanVienRepository.delete(deleteNhanVien);

			return new ResponseEntity<>(nhanVienRepository.findByPhongBanId(phongbanid), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping("/phongban/{phongbanid}/nhanvien")
	public ResponseEntity<Object> deleteAllNhanVien(@PathVariable(required = false) Long phongbanid) {
		try {
			if (phongbanid == null) {

				nhanVienRepository.deleteAll();

				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}

			if (!phongBanRepository.existsById(phongbanid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			Collection<CNhanVien> danhSachNhanVienPhongBan = nhanVienRepository.findByPhongBanId(phongbanid);

			for (CNhanVien cNhanVien : danhSachNhanVienPhongBan) {
				nhanVienRepository.delete(cNhanVien);
			}

			return new ResponseEntity<>(nhanVienRepository.findByPhongBanId(phongbanid), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}
}
