package com.devcamp.company.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "phong_ban")
public class CPhongBan {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank(message = "Mã phòng ban không được để trống")
	@Column(nullable = false, unique = true)
	private String maPhongBan;

	@NotBlank(message = "Tên phòng ban không được để trống")
	@Column(nullable = false)
	private String tenPhongBan;

	@NotBlank(message = "Nghiệp vụ không được để trống")
	@Column(nullable = false)
	private String nghiepVu;

	private String gioiThieu;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "phongBan")
	private List<CNhanVien> danhSachNhanVien;

	public CPhongBan() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CPhongBan(Long id, @NotBlank(message = "Mã phòng ban không được để trống") String maPhongBan,
			@NotBlank(message = "Tên phòng ban không được để trống") String tenPhongBan,
			@NotBlank(message = "Nghiệp vụ không được để trống") String nghiepVu, String gioiThieu,
			List<CNhanVien> danhSachNhanVien) {
		super();
		this.id = id;
		this.maPhongBan = maPhongBan;
		this.tenPhongBan = tenPhongBan;
		this.nghiepVu = nghiepVu;
		this.gioiThieu = gioiThieu;
		this.danhSachNhanVien = danhSachNhanVien;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMaPhongBan() {
		return maPhongBan;
	}

	public void setMaPhongBan(String maPhongBan) {
		this.maPhongBan = maPhongBan;
	}

	public String getTenPhongBan() {
		return tenPhongBan;
	}

	public void setTenPhongBan(String tenPhongBan) {
		this.tenPhongBan = tenPhongBan;
	}

	public String getNghiepVu() {
		return nghiepVu;
	}

	public void setNghiepVu(String nghiepVu) {
		this.nghiepVu = nghiepVu;
	}

	public String getGioiThieu() {
		return gioiThieu;
	}

	public void setGioiThieu(String gioiThieu) {
		this.gioiThieu = gioiThieu;
	}

	public List<CNhanVien> getDanhSachNhanVien() {
		return danhSachNhanVien;
	}

	public void setDanhSachNhanVien(List<CNhanVien> danhSachNhanVien) {
		this.danhSachNhanVien = danhSachNhanVien;
	}

}
