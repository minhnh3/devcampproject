package com.devcamp.company.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "nhan_vien")
public class CNhanVien {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank(message = "Mã nhân viên không được để trống")
	@Column(nullable = false, unique = true)
	private String maNhanVien;

	@NotBlank(message = "Tên nhân viên không được để trống")
	@Column(nullable = false)
	private String tenNhanVien;

	@NotBlank(message = "Chức vụ không được để trống")
	@Column(nullable = false)
	private String chucVu;

	@NotBlank(message = "Giới tính không được để trống")
	@Column(nullable = false)
	private String gioiTinh;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd/MM/yyyy")
	@Column(nullable = false, updatable = false)
	private Date ngaySinh;

	@NotBlank(message = "Địa chỉ không được để trống")
	@Column(nullable = false)
	private String diaChi;

	@Pattern(regexp = "(^$|[0-9]{10})", message = "Số điện thoại không hợp lệ")
	@NotBlank(message = "Số điện thoại không được để trống")
	@Column(nullable = false, unique = true)
	private String soDienThoai;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private CPhongBan phongBan;

	public CNhanVien() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CNhanVien(Long id, @NotBlank(message = "Mã nhân viên không được để trống") String maNhanVien,
			@NotBlank(message = "Tên nhân viên không được để trống") String tenNhanVien,
			@NotBlank(message = "Chức vụ không được để trống") String chucVu,
			@NotBlank(message = "Giới tính không được để trống") String gioiTinh, Date ngaySinh,
			@NotBlank(message = "Địa chỉ không được để trống") String diaChi,
			@Pattern(regexp = "(^$|[0-9]{10})", message = "Số điện thoại không hợp lệ") @NotBlank(message = "Số điện thoại không được để trống") String soDienThoai,
			CPhongBan phongBan) {
		super();
		this.id = id;
		this.maNhanVien = maNhanVien;
		this.tenNhanVien = tenNhanVien;
		this.chucVu = chucVu;
		this.gioiTinh = gioiTinh;
		this.ngaySinh = ngaySinh;
		this.diaChi = diaChi;
		this.soDienThoai = soDienThoai;
		this.phongBan = phongBan;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMaNhanVien() {
		return maNhanVien;
	}

	public void setMaNhanVien(String maNhanVien) {
		this.maNhanVien = maNhanVien;
	}

	public String getTenNhanVien() {
		return tenNhanVien;
	}

	public void setTenNhanVien(String tenNhanVien) {
		this.tenNhanVien = tenNhanVien;
	}

	public String getChucVu() {
		return chucVu;
	}

	public void setChucVu(String chucVu) {
		this.chucVu = chucVu;
	}

	public String getGioiTinh() {
		return gioiTinh;
	}

	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}

	public Date getNgaySinh() {
		return ngaySinh;
	}

	public void setNgaySinh(Date ngaySinh) {
		this.ngaySinh = ngaySinh;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public String getSoDienThoai() {
		return soDienThoai;
	}

	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}

	public CPhongBan getPhongBan() {
		return phongBan;
	}

	public void setPhongBan(CPhongBan phongBan) {
		this.phongBan = phongBan;
	}

}
