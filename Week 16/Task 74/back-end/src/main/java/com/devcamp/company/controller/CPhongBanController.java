package com.devcamp.company.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.company.model.CPhongBan;
import com.devcamp.company.repository.IPhongBanRepository;

@RestController
@CrossOrigin
public class CPhongBanController {
	@Autowired
	IPhongBanRepository phongBanRepository;

	@GetMapping("/phongban")
	public ResponseEntity<Object> getAllPhongBan() {
		try {
			return new ResponseEntity<>(phongBanRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@GetMapping("/phongban/{id}")
	public ResponseEntity<Object> getPhongBanById(@PathVariable Long id) {
		try {
			if (!phongBanRepository.existsById(id)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<>(phongBanRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@PostMapping("/phongban")
	public ResponseEntity<Object> createPhongBan(@Valid @RequestBody CPhongBan cPhongBan) {
		try {
			CPhongBan newPhongBan = new CPhongBan();
			newPhongBan.setMaPhongBan(cPhongBan.getMaPhongBan().trim());
			newPhongBan.setTenPhongBan(cPhongBan.getTenPhongBan().trim());
			newPhongBan.setNghiepVu(cPhongBan.getNghiepVu().trim());
			newPhongBan.setGioiThieu(cPhongBan.getGioiThieu());
			if (cPhongBan.getGioiThieu() != null) {
				newPhongBan.setGioiThieu(cPhongBan.getGioiThieu().trim());
			}

			return new ResponseEntity<>(phongBanRepository.save(newPhongBan), HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/phongban/{id}")
	public ResponseEntity<Object> updatePhongBan(@PathVariable Long id, @Valid @RequestBody CPhongBan cPhongBan) {
		try {
			if (!phongBanRepository.existsById(id)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			CPhongBan updatePhongBan = phongBanRepository.findById(id).get();
			updatePhongBan.setMaPhongBan(cPhongBan.getMaPhongBan().trim());
			updatePhongBan.setTenPhongBan(cPhongBan.getTenPhongBan().trim());
			updatePhongBan.setNghiepVu(cPhongBan.getNghiepVu().trim());
			updatePhongBan.setGioiThieu(cPhongBan.getGioiThieu().trim());

			return new ResponseEntity<>(phongBanRepository.save(updatePhongBan), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping("/phongban/{id}")
	public ResponseEntity<Object> deletePhongBanById(@PathVariable Long id) {
		try {
			if (!phongBanRepository.existsById(id)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			phongBanRepository.deleteById(id);

			return new ResponseEntity<>(null, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping("/phongban")
	public ResponseEntity<Object> deleteAllPhongBan() {
		try {
			phongBanRepository.deleteAll();

			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}
}
