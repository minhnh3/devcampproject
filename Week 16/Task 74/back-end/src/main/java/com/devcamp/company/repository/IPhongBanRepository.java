package com.devcamp.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.company.model.CPhongBan;

public interface IPhongBanRepository extends JpaRepository<CPhongBan, Long> {
	CPhongBan findByMaPhongBan(String maPhongBan);
}
