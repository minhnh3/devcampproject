package com.devcamp.company.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.company.model.CNhanVien;

public interface INhanVienRepository extends JpaRepository<CNhanVien, Long> {
	Collection<CNhanVien> findByPhongBanId(Long id);

	CNhanVien findByIdAndPhongBanId(Long nhanvienid, Long phongbanid);
}
