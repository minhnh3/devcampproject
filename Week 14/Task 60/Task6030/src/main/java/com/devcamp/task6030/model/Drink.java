package com.devcamp.task6030.model;

public class Drink {
	private int ordinalNumber;
	private String drinkId;
	private String drinkName;
	private int price;
	private String createdDate;
	private String updateddDate;

	public Drink() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Drink(int ordinalNumber, String drinkId, String drinkName, int price, String createdDate,
			String updateddDate) {
		super();
		this.ordinalNumber = ordinalNumber;
		this.drinkId = drinkId;
		this.drinkName = drinkName;
		this.price = price;
		this.createdDate = createdDate;
		this.updateddDate = updateddDate;
	}

	public int getOrdinalNumber() {
		return ordinalNumber;
	}

	public void setOrdinalNumber(int ordinalNumber) {
		this.ordinalNumber = ordinalNumber;
	}

	public String getDrinkId() {
		return drinkId;
	}

	public void setDrinkId(String drinkId) {
		this.drinkId = drinkId;
	}

	public String getDrinkName() {
		return drinkName;
	}

	public void setDrinkName(String drinkName) {
		this.drinkName = drinkName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateddDate() {
		return updateddDate;
	}

	public void setUpdateddDate(String updateddDate) {
		this.updateddDate = updateddDate;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{ordinalNumber:" + this.getOrdinalNumber() + ", drinkId:" + this.getDrinkId() + ", drinkName:"
				+ this.getDrinkName() + ", price:" + this.getPrice() + ", createdDate:" + this.getCreatedDate()
				+ ", updateddDate:" + this.getUpdateddDate() + "}";
	}
}
