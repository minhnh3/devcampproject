package com.devcamp.task6030;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task6030Application {

	public static void main(String[] args) {
		SpringApplication.run(Task6030Application.class, args);
	}

}
