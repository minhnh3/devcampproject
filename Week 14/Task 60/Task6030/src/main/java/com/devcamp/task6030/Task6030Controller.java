package com.devcamp.task6030;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task6030.model.Drink;
import com.devcamp.task6030.model.ListDrinks;

@RestController
public class Task6030Controller {
	@CrossOrigin
	@GetMapping("/listdrinks")
	public ListDrinks getListDrinks() {
		Drink vTraTac = new Drink(1, "TRATAC", "Trà tắc", 10000, "14/05/2021", "14/05/2021");
		Drink vCoca = new Drink(2, "COCA", "Cocacola", 15000, "14/05/2021", "14/05/2021");
		Drink vPepsi = new Drink(3, "PEPSI", "Pepsi", 15000, "14/05/2021", "14/05/2021");
		ListDrinks vListDrinks = new ListDrinks(new ArrayList<Drink>(Arrays.asList(vTraTac, vCoca, vPepsi)));
		return vListDrinks;
	}

	public static void main(String[] args) {
		Drink vTraTac = new Drink(1, "TRATAC", "Trà tắc", 10000, "14/05/2021", "14/05/2021");
		Drink vCoca = new Drink(2, "COCA", "Cocacola", 15000, "14/05/2021", "14/05/2021");
		Drink vPepsi = new Drink(3, "PEPSI", "Pepsi", 15000, "14/05/2021", "14/05/2021");
		System.out.println(vTraTac);
		System.out.println(vCoca);
		System.out.println(vPepsi);
	}
}
