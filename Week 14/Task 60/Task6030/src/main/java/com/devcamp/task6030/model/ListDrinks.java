package com.devcamp.task6030.model;

import java.util.ArrayList;

public class ListDrinks {
	private ArrayList<Drink> listDrinks;

	public ListDrinks() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ListDrinks(ArrayList<Drink> listDrinks) {
		super();
		this.listDrinks = listDrinks;
	}

	public ArrayList<Drink> getListDrinks() {
		return listDrinks;
	}

	public void setListDrinks(ArrayList<Drink> listDrinks) {
		this.listDrinks = listDrinks;
	}

}
