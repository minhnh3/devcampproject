package com.devcamp.task6040.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task6040.model.CCountry;
import com.devcamp.task6040.model.CRegion;

@RestController
public class Task6040Controller {
	private final int VIETNAM = 1;
	private final int USA = 2;

	@CrossOrigin
	@GetMapping("/listcountry")
	public List<CCountry> getListCountry(
			@RequestParam(value = "countryCode", defaultValue = "0") String paramCountryCode) {
		CRegion vHanoi = new CRegion("1", "Hà Nội");
		CRegion vDanang = new CRegion("2", "Đà Nẵng");
		CRegion vHochiminh = new CRegion("3", "Hồ Chí Minh");
		CCountry vVietNam = new CCountry("1", "Việt Nam",
				new ArrayList<CRegion>(Arrays.asList(vHanoi, vDanang, vHochiminh)));

		CRegion vNewYork = new CRegion("1", "NewYork");
		CRegion vCalifornia = new CRegion("2", "California");
		CRegion vMexico = new CRegion("3", "Mexico");
		CCountry vUSA = new CCountry("2", "USA", new ArrayList<CRegion>(Arrays.asList(vNewYork, vCalifornia, vMexico)));

		ArrayList<CCountry> vListCountry = new ArrayList<CCountry>(Arrays.asList(vVietNam, vUSA));
		ArrayList<CCountry> vFilterListCountry = new ArrayList<CCountry>();

		int vCountryCode = Integer.parseInt(paramCountryCode);
		if (vCountryCode == 0) {
			vFilterListCountry = vListCountry;
		}
		if (vCountryCode == VIETNAM) {
			vFilterListCountry.add(vVietNam);
		}
		if (vCountryCode == USA) {
			vFilterListCountry.add(vUSA);
		}

		return vFilterListCountry;
	}

	public static void main(String[] args) {
		Task6040Controller vTask6040Controller = new Task6040Controller();
		System.out.println(vTask6040Controller.getListCountry("0"));
	}
}
