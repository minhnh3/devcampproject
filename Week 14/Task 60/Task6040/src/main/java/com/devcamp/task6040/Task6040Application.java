package com.devcamp.task6040;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task6040Application {

	public static void main(String[] args) {
		SpringApplication.run(Task6040Application.class, args);
	}

}
