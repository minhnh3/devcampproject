package com.devcamp.task5640;

public abstract class Animals implements IAnimal {
	private int age;
	private String gender;

	public Animals() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Animals(int age, String gender) {
		super();
		this.age = age;
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isMammal() {
		return false;
	}

	public void mate() {
		System.out.println("Animals mating");
	}
}
