package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5640Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5640Application.class, args);
	}

}
