package com.devcamp.task5640;

public interface IAnimal {
	public void animalSound();

	public void sleep();
}
