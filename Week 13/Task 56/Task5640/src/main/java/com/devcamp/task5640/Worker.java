package com.devcamp.task5640;

import java.util.ArrayList;

public class Worker extends Person {
	private int salary;

	public Worker() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Worker(int age, String gender, String name, Address address, ArrayList<Animals> listPet, int salary) {
		super(age, gender, name, address, listPet);
		// TODO Auto-generated constructor stub
		this.salary = salary;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Worker sound");
	}

	@Override
	public void sleep() {
		// TODO Auto-generated method stub
		System.out.println("Worker sound");
	}

	@Override
	public void gotoShop() {
		// TODO Auto-generated method stub
		System.out.println("Worker sound");
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Worker sound");
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Worker eating");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Worker:{age:" + this.getAge() + ", gender:" + this.getGender() + ", name:" + this.getName()
				+ ", address:" + this.getAddress() + ", listPet:" + this.getListPet() + ", salary:" + this.salary + "}"
				+ "\n";
	}

	public void working() {
		System.out.println("Worker working");
	}
}
