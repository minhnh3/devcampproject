package com.devcamp.task5620.animals;

import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		ArrayList<Animals> vListAnimal = new ArrayList<Animals>();
		Duck vDuck = new Duck(1, "male", "yellow");
		Fish vFish = new Fish(2, "female", 22, true);
		Zebra vZebra = new Zebra(3, "male", true);
		vListAnimal.add(vDuck);
		vListAnimal.add(vFish);
		vListAnimal.add(vZebra);
		System.out.println(vListAnimal);
	}
}
