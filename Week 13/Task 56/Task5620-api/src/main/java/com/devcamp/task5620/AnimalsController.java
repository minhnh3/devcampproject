package com.devcamp.task5620;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5620.animals.Animals;
import com.devcamp.task5620.animals.Duck;
import com.devcamp.task5620.animals.Fish;
import com.devcamp.task5620.animals.Zebra;

@RestController
public class AnimalsController {
	@CrossOrigin
	@GetMapping("/animalslist")
	public List<Animals> getAnimalsList() {
		ArrayList<Animals> vAnimalsList = new ArrayList<Animals>();
		Duck vDuck = new Duck(1, "male", "yellow");
		Fish vFish = new Fish(2, "female", 22, true);
		Zebra vZebra = new Zebra(3, "male", true);
		vAnimalsList.add(vDuck);
		vAnimalsList.add(vFish);
		vAnimalsList.add(vZebra);
		return vAnimalsList;
	}

}
