package com.devcamp.task5620;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5620ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5620ApiApplication.class, args);
	}

}
