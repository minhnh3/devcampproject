package com.devcamp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5610.Address;
import com.devcamp.task5610.Person;
import com.devcamp.task5610.Professor;
import com.devcamp.task5610.Student;
import com.devcamp.task5610.Subject;

@RestController
public class PersonController {
	@CrossOrigin
	@GetMapping
	public List<Person> getListPerson() {
		// create List Student
		ArrayList<Person> vListStudent = new ArrayList<Person>();
		// create Professor
		Professor vLayla = new Professor(50, "female", "Layla",
				new Address("Nguyễn Huệ", "Hồ Chí Minh", "Việt Nam", 100000), 30000);
		Professor vJoe = new Professor(45, "male", "Joe", new Address("Rayban", "New Castle", "England", 150000),
				20000);
		Professor vJayChou = new Professor(55, "male", "JayChou",
				new Address("Tam Trinh", "Đà Nẵng", "Việt Nam", 100000), 35000);
		Professor vKim = new Professor(40, "male", "Kim", new Address("New Square", "New York", "USA", 200000), 20000);
		// create Subject
		Subject vHTML = new Subject("HTML", 22, vLayla);
		Subject vCSS = new Subject("CSS", 33, vJoe);
		Subject vJS = new Subject("JS", 44, vJayChou);
		Subject vPython = new Subject("Python", 55, vJayChou);
		Subject vJava = new Subject("Java", 11, vKim);
		// create Student
		Student vMinh = new Student(30, "male", "Minh", new Address("Kim Ngưu", "Hà Nội", "Việt Nam", 100000), 7,
				new ArrayList<Subject>() {
					{
						add(new Subject("Java", 11, new Professor(40, "male", "Nam",
								new Address("Kim Ngưu", "Hà Nội", "Việt Nam", 100000), 15000)));
						add(vHTML);
						add(vCSS);
					}
				});

		ArrayList<Subject> vDuySubject = new ArrayList<Subject>(Arrays.asList(vHTML, vCSS, vJS));
		vDuySubject.add(vJava);
		Student vDuy = new Student(21, "male", "Duy", new Address("Tam Trinh", "Hà Nội", "Việt Nam", 100000), 3,
				vDuySubject);
		vDuy.addSubject(vPython);
		// add Student to List
		vListStudent.add(vMinh);
		vListStudent.add(vDuy);

		return vListStudent;
	}

	public static void main(String[] args) {
		PersonController vPersonController = new PersonController();
		System.out.println(vPersonController.getListPerson());
	}
}
