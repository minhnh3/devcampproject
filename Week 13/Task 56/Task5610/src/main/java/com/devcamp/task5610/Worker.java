package com.devcamp.task5610;

public class Worker extends Person {
	private int salary;

	public Worker() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Worker(int age, String gender, String name, Address address, int salary) {
		super(age, gender, name, address);
		// TODO Auto-generated constructor stub
		this.salary = salary;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Worker eating");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Worker:{age:" + this.getAge() + ", gender:" + this.getGender() + ", name:" + this.getName()
				+ ", address:" + this.getAddress() + ", salary:" + this.salary + "}";
	}

	public void working() {
		System.out.println("Worker working");
	}
}
