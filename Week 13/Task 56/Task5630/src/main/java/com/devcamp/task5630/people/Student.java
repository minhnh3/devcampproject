package com.devcamp.task5630.people;

import java.util.ArrayList;

import com.devcamp.task5630.animals.Animals;

public class Student extends Person {
	private int studentId;
	private ArrayList<Subject> listSubject;

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(int age, String gender, String name, Address address, ArrayList<Animals> listPet, int studentId,
			ArrayList<Subject> listSubject) {
		super(age, gender, name, address, listPet);
		// TODO Auto-generated constructor stub
		this.studentId = studentId;
		this.listSubject = listSubject;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public ArrayList<Subject> getListSubject() {
		return listSubject;
	}

	public void setListSubject(ArrayList<Subject> listSubject) {
		this.listSubject = listSubject;
	}

	public void addSubject(Subject subject) {
		this.listSubject.add(subject);
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Student eating");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Student:{age:" + this.getAge() + ", gender:" + this.getGender() + ", name:" + this.getName()
				+ ", address:" + this.getAddress() + ", listPet:" + this.getListPet() + ", studentId:" + this.studentId
				+ ", listSubject:" + this.listSubject + "}" + "\n";
	}

	public void doHomework() {
		System.out.println("Student doing homework");
	}
}
