package com.devcamp.task5630.animals;

public abstract class Animals {
	private int age;
	private String gender;

	public Animals() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Animals(int age, String gender) {
		super();
		this.age = age;
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public abstract boolean isMammal();

	public abstract void mate();
}
