package com.devcamp.task5630.animals;

public class Zebra extends Animals {
	private boolean is_wild;

	public Zebra() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Zebra(int age, String gender, boolean is_wild) {
		super(age, gender);
		// TODO Auto-generated constructor stub
		this.is_wild = is_wild;
	}

	public boolean isIs_wild() {
		return is_wild;
	}

	public void setIs_wild(boolean is_wild) {
		this.is_wild = is_wild;
	}

	@Override
	public boolean isMammal() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void mate() {
		// TODO Auto-generated method stub
		System.out.println("Zebra mate...");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Zebra{age:" + this.getAge() + ", gender:" + this.getGender() + ", is_wild:" + this.is_wild + "}";
	}

	public void run() {
		System.out.println("Zebra running");
	}
}
