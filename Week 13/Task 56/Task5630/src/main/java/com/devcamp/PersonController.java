package com.devcamp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5630.animals.Animals;
import com.devcamp.task5630.animals.Duck;
import com.devcamp.task5630.animals.Fish;
import com.devcamp.task5630.animals.Zebra;
import com.devcamp.task5630.people.Address;
import com.devcamp.task5630.people.Person;
import com.devcamp.task5630.people.Professor;
import com.devcamp.task5630.people.Student;
import com.devcamp.task5630.people.Subject;
import com.devcamp.task5630.people.Worker;

@RestController
public class PersonController {
	private final int STUDENT = 1;
	private final int WORKER = 2;
	private final int PROFESSOR = 3;

	@CrossOrigin
	@GetMapping("/listpeople")
	public List<Person> getListPeople(@RequestParam(value = "type", defaultValue = "0") String paramType) {
		// create List People
		ArrayList<Person> vListPeople = new ArrayList<Person>();
		// create Professor
		Animals vLayLaDuckPet = new Duck(5, "male", "white");
		Professor vLayla = new Professor(50, "female", "Layla",
				new Address("Nguyễn Huệ", "Hồ Chí Minh", "Việt Nam", 100000),
				new ArrayList<Animals>(Arrays.asList(vLayLaDuckPet)), 30000);
		Animals vJoeFishPet = new Fish(1000, "male", 1000, true);
		Animals vJoeZebraPet = new Zebra(5000, "male", true);
		Professor vJoe = new Professor(45, "male", "Joe", new Address("Rayban", "New Castle", "England", 150000),
				new ArrayList<Animals>(Arrays.asList(vJoeFishPet, vJoeZebraPet)), 20000);
		Professor vJayChou = new Professor(55, "male", "JayChou",
				new Address("Tam Trinh", "Đà Nẵng", "Việt Nam", 100000), new ArrayList<Animals>(Arrays.asList()),
				35000);
		Professor vKim = new Professor(40, "male", "Kim", new Address("New Square", "New York", "USA", 200000),
				new ArrayList<Animals>(Arrays.asList()), 20000);
		// create Subject
		Subject vHTML = new Subject("HTML", 22, vLayla);
		Subject vCSS = new Subject("CSS", 33, vJoe);
		Subject vJS = new Subject("JS", 44, vJayChou);
		Subject vPython = new Subject("Python", 55, vJayChou);
		Subject vJava = new Subject("Java", 11, vKim);
		// create Student
		Animals vMinhDuckPet = new Duck(1, "female", "yellow");
		Animals vMinhZebraPet = new Zebra(7, "female", true);
		Student vMinh = new Student(30, "male", "Minh", new Address("Kim Ngưu", "Hà Nội", "Việt Nam", 100000),
				new ArrayList<Animals>(Arrays.asList(vMinhDuckPet, vMinhZebraPet)), 7, new ArrayList<Subject>() {
					{
						add(new Subject("Java", 11,
								new Professor(40, "male", "Nam", new Address("Kim Ngưu", "Hà Nội", "Việt Nam", 100000),
										new ArrayList<Animals>(Arrays.asList()), 15000)));
						add(vHTML);
						add(vCSS);
					}
				});

		Animals vDuyFishPet = new Fish(1, "male", 11, true);
		ArrayList<Subject> vDuySubject = new ArrayList<Subject>(Arrays.asList(vHTML, vCSS, vJS));
		vDuySubject.add(vJava);
		Student vDuy = new Student(21, "male", "Duy", new Address("Tam Trinh", "Hà Nội", "Việt Nam", 100000),
				new ArrayList<Animals>(Arrays.asList(vDuyFishPet)), 3, vDuySubject);
		vDuy.addSubject(vPython);
		// create Worker
		Animals vQuangFishPet = new Fish(1, "female", 13, true);
		Person vQuang = new Worker(26, "male", "Quang", new Address("Cầu Giấy", "Hà Nội", "Việt Nam", 100000),
				new ArrayList<Animals>(Arrays.asList(vQuangFishPet)), 500);
		Animals vDucZebraPet = new Zebra(5, "male", true);
		Person vDuc = new Worker(29, "male", "Duc", new Address("Trường Chinh", "Hà Nội", "Việt Nam", 100000),
				new ArrayList<Animals>(Arrays.asList(vDucZebraPet)), 500);
		// add People to List
		vListPeople.add(vMinh);
		vListPeople.add(vDuy);
		vListPeople.add(vQuang);
		vListPeople.add(vDuc);
		vListPeople.add(vLayla);
		vListPeople.add(vJoe);
		// create List People Filter
		int vPersonType = Integer.parseInt(paramType);
		ArrayList<Person> vFilterListPeople = new ArrayList<Person>();
		if (vPersonType == STUDENT || vPersonType == WORKER || vPersonType == PROFESSOR) {
			for (int i = 0; i < vListPeople.size(); i++) {
				if (vPersonType == STUDENT && vListPeople.get(i) instanceof Student) {
					vFilterListPeople.add(vListPeople.get(i));
				}
				if (vPersonType == WORKER && vListPeople.get(i) instanceof Worker) {
					vFilterListPeople.add(vListPeople.get(i));
				}
				if (vPersonType == PROFESSOR && vListPeople.get(i) instanceof Professor) {
					vFilterListPeople.add(vListPeople.get(i));
				}
			}
		} else {
			vFilterListPeople = vListPeople;
		}
		return vFilterListPeople;
	}

	public static void main(String[] args) {
		PersonController vPersonController = new PersonController();
		System.out.println(vPersonController.getListPeople("4"));
	}
}
