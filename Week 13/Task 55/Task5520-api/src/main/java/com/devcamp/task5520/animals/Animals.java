package com.devcamp.task5520.animals;

public class Animals {
	private int age;
	private String gender;

	public Animals() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Animals(int age, String gender) {
		super();
		this.age = age;
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Animal:{age:" + this.age + ", gender:" + this.gender + "}";
	}

	public boolean isMammal() {
		return true;
	}

	public void mate() {
		System.out.println("Animals mate...");
	}
}
