package com.devcamp.task5520.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5520ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5520ApiApplication.class, args);
	}

}
