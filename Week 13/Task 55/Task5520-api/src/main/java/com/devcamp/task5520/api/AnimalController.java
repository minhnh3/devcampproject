package com.devcamp.task5520.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5520.animals.Animals;
import com.devcamp.task5520.animals.Duck;
import com.devcamp.task5520.animals.Fish;
import com.devcamp.task5520.animals.Zebra;

@RestController
public class AnimalController {
	public static void main(String[] args) {
		ArrayList<Animals> vAnimalsList = new ArrayList<Animals>();
		Animals vMonkeyAnimal = new Animals(30, "male");
		Duck vDuck = new Duck(2, "male", "yellow");
		Fish vFish = new Fish(4, "female", 44, true);
		Zebra vZebra = new Zebra(3, "male", true);
		vAnimalsList.add(vMonkeyAnimal);
		vAnimalsList.add(vDuck);
		vAnimalsList.add(vFish);
		vAnimalsList.add(vZebra);
		System.out.println(vAnimalsList);
	}

	@CrossOrigin
	@GetMapping("/listAnimal")
	public List<Animals> getAnimalsList() {
		ArrayList<Animals> vAnimalsList = new ArrayList<Animals>();
		Animals vMonkeyAnimal = new Animals(30, "male");
		Duck vDuck = new Duck(2, "male", "yellow");
		Fish vFish = new Fish(4, "female", 44, true);
		Zebra vZebra = new Zebra(3, "male", true);
		vAnimalsList.add(vMonkeyAnimal);
		vAnimalsList.add(vDuck);
		vAnimalsList.add(vFish);
		vAnimalsList.add(vZebra);
		return vAnimalsList;
	}

}
