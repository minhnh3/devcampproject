package com.devcamp.task5520.animals;

public class Duck extends Animals {
	private String beakColor;

	public Duck() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Duck(int age, String gender, String beakColor) {
		super(age, gender);
		// TODO Auto-generated constructor stub
		this.beakColor = beakColor;
	}

	public String getBeakColor() {
		return beakColor;
	}

	public void setBeakColor(String beakColor) {
		this.beakColor = beakColor;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Duck:{age:" + this.getAge() + ", gender:" + this.getGender() + ", beakColor:" + this.beakColor + "}";
	}

	@Override
	public boolean isMammal() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mate() {
		// TODO Auto-generated method stub
		super.mate();
		System.out.println("Duck mate...");
	}

	public void swim() {
		System.out.println("Duck swimming");
	}

	public void quack() {
		System.out.println("Duck quacking");
	}
}
