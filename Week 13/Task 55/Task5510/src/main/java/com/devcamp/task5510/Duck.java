package com.devcamp.task5510;

public class Duck {
	private int age;
	private String gender;
	private String beakColor;

	public Duck() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Duck(int age, String gender, String beakColor) {
		super();
		this.age = age;
		this.gender = gender;
		this.beakColor = beakColor;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBeakColor() {
		return beakColor;
	}

	public void setBeakColor(String beakColor) {
		this.beakColor = beakColor;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Duck:{age:" + this.age + ", gender:" + this.gender + ", beakColor:" + this.beakColor + "}";
	}

	public boolean isMammal() {
		return false;
	}

	public void mate() {
		System.out.println("Duck mate...");
	}

	public void swim() {
		System.out.println("Duck swimming");
	}

	public void quack() {
		System.out.println("Duck quacking");
	}

}
