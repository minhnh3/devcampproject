package com.devcamp.task5510;

public class Fish {
	private int age;
	private String gender;
	private int size;
	private boolean canEat;

	public Fish() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Fish(int age, String gender, int size, boolean canEat) {
		super();
		this.age = age;
		this.gender = gender;
		this.size = size;
		this.canEat = canEat;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isCanEat() {
		return canEat;
	}

	public void setCanEat(boolean canEat) {
		this.canEat = canEat;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Fish:{age:" + this.age + ", gender:" + this.gender + ", size:" + this.size + ", canEat:" + this.canEat
				+ "}";
	}

	public boolean isMammal() {
		return false;
	}

	public void mate() {
		System.out.println("Fish mate...");
	}

	public void swim() {
		System.out.println("Fish swimming");
	}

}
