package com.devcamp.task5510;

public class Zebra {
	private int age;
	private String gender;
	private boolean is_wild;

	public Zebra() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Zebra(int age, String gender, boolean is_wild) {
		super();
		this.age = age;
		this.gender = gender;
		this.is_wild = is_wild;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isIs_wild() {
		return is_wild;
	}

	public void setIs_wild(boolean is_wild) {
		this.is_wild = is_wild;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Zebra:{age:" + this.age + ", gender:" + this.gender + ", is_wild:" + this.is_wild + "}";
	}

	public boolean isMammal() {
		return true;
	}

	public void mate() {
		System.out.println("Zebra mate...");
	}

	public void run() {
		System.out.println("Zebra running");
	}

}
