package com.devcamp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5510.Duck;
import com.devcamp.task5510.Fish;
import com.devcamp.task5510.Zebra;

@RestController
public class AnimalController {
	public static void main(String[] args) {
		Duck vDuck = new Duck(2, "male", "yellow");
		Fish vFish = new Fish(4, "female", 44, true);
		Zebra vZebra = new Zebra(3, "male", true);

		System.out.println(vDuck);
		System.out.println(vFish);
		System.out.println(vZebra);
	}

	@CrossOrigin
	@GetMapping("/ducks")
	public List<Duck> getDuckList() {
		Duck vDuck01 = new Duck(2, "male", "yellow");
		Duck vDuck02 = new Duck(1, "female", "white");
		Duck vDuck03 = new Duck(5, "male", "red");
		ArrayList<Duck> vDuckList = new ArrayList<Duck>();
		vDuckList.add(vDuck03);
		vDuckList.add(vDuck01);
		vDuckList.add(vDuck02);
		return vDuckList;
	}

	@CrossOrigin
	@GetMapping("/fishes")
	public List<Fish> getFishList() {
		Fish vFish01 = new Fish(5, "female", 55, true);
		Fish vFish02 = new Fish(11, "female", 111, true);
		Fish vFish03 = new Fish(55, "male", 555, true);
		ArrayList<Fish> vFishList = new ArrayList<Fish>();
		vFishList.add(vFish03);
		vFishList.add(vFish01);
		return vFishList;
	}

	@CrossOrigin
	@GetMapping("/zebras")
	public List<Zebra> getZebraList() {
		Zebra vZebra01 = new Zebra(333, "male", true);
		Zebra vZebra02 = new Zebra(111, "male", true);
		Zebra vZebra03 = new Zebra(777, "female", true);
		ArrayList<Zebra> vZebraList = new ArrayList<Zebra>();
		vZebraList.add(vZebra02);
		vZebraList.add(vZebra02);
		vZebraList.add(vZebra01);
		return vZebraList;
	}
}
