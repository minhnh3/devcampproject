package com.devcamp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5530.Address;
import com.devcamp.task5530.Person;
import com.devcamp.task5530.Professor;
import com.devcamp.task5530.Student;

@RestController
public class PersonController {
	@CrossOrigin
	@GetMapping
	public List<Person> getPersonList() {
		ArrayList<Person> vPersonList = new ArrayList<Person>();
		Person vPerson = new Person(10, "female", "Lisa", new Address("Lò Đúc", "Hà Nội", "Việt Nam", 100000));
		Student vStudent = new Student(20, "male", "Yoa", new Address("Kim Ngưu", "Hà Nội", "Việt Nam", 100000), 2, 8);
		Person vPersonProfessor = new Professor(50, "female", "Young",
				new Address("Thành Thái", "Hồ Chí Minh", "Việt Nam", 100000), 10000);
		vPersonList.add(vPerson);
		vPersonList.add(vStudent);
		vPersonList.add(vPersonProfessor);
		return vPersonList;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PersonController vPersonController = new PersonController();
		System.out.println(vPersonController.getPersonList());
	}

}
