package com.devcamp.task5530;

public class Student extends Person {
	private int studentId;
	private float averageMark;

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(int age, String gender, String name, Address address, int studentId, float averageMark) {
		super(age, gender, name, address);
		// TODO Auto-generated constructor stub
		this.studentId = studentId;
		this.averageMark = averageMark;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public float getAverageMark() {
		return averageMark;
	}

	public void setAverageMark(float averageMark) {
		this.averageMark = averageMark;
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Student eating");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Student:{age:" + this.getAge() + ", gender:" + this.getGender() + ", name:" + this.getName()
				+ ", address:" + this.getAddress() + ", studentId:" + this.studentId + ", averageMark:"
				+ this.averageMark + "}";
	}

	public void doHomework() {
		System.out.println("Student doing homework");
	}
}
