package com.devcamp.task5535;

public class Professor extends Person {
	private int salary;

	public Professor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Professor(int age, String gender, String name, Address address, int salary) {
		super(age, gender, name, address);
		// TODO Auto-generated constructor stub
		this.salary = salary;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Professor eating");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Professor:{age:" + this.getAge() + ", gender:" + this.getGender() + ", name:" + this.getName()
				+ ", address:" + this.getAddress() + ", salary:" + this.salary + "}";
	}

	public void teaching() {
		System.out.println("Professor teaching");
	}
}
