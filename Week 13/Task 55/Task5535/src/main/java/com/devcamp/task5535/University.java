package com.devcamp.task5535;

import java.util.ArrayList;

public class University {
	private int code;
	private String name;
	private ArrayList<Person> listPerson;

	public University() {
		super();
		// TODO Auto-generated constructor stub
	}

	public University(int code, String name, ArrayList<Person> listPerson) {
		super();
		this.code = code;
		this.name = name;
		this.listPerson = listPerson;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Person> getListPerson() {
		return listPerson;
	}

	public void setListPerson(ArrayList<Person> listPerson) {
		this.listPerson = listPerson;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "University:{code:" + this.code + ", name:" + this.name + ", listPerson:" + this.listPerson + "}";
	}

}
