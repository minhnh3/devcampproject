package com.devcamp;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5535.Address;
import com.devcamp.task5535.Person;
import com.devcamp.task5535.Professor;
import com.devcamp.task5535.Student;
import com.devcamp.task5535.University;

@RestController
public class UniversityController {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		University vUniversity = new University(1, "VNU", new ArrayList<Person>() {
			{
				add(new Person(17, "female", "Nga", new Address("Tam Trinh", "Hà Nội", "Việt Nam", 100000)));
				add(new Student(20, "male", "Minh", new Address("Kim Ngưu", "Hà Nội", "Việt Nam", 100000), 33, 9.9f));
				add(new Professor(40, "female", "Linh", new Address("Nguyễn Huệ", "Hồ Chí Minh", "Việt Nam", 100000),
						10000));
			}
		});
		System.out.println(vUniversity);
	}

	@CrossOrigin
	@GetMapping
	public University getUniversityInformation() {
		ArrayList<Person> vListPerson = new ArrayList<Person>();
		University vUniversity = new University(1, "VNU", vListPerson);
		Person vPersonProfessor = new Professor(40, "female", "Linh",
				new Address("Nguyễn Huệ", "Hồ Chí Minh", "Việt Nam", 100000), 10000);
		Person vPerson = new Person(17, "female", "Nga", new Address("Tam Trinh", "Hà Nội", "Việt Nam", 100000));
		Person vPersonStudent = new Student(20, "male", "Minh", new Address("Kim Ngưu", "Hà Nội", "Việt Nam", 100000),
				33, 9.9f);
		vListPerson.add(vPersonProfessor);
		vListPerson.add(vPerson);
		vListPerson.add(vPersonStudent);
		return vUniversity;
	}

}
