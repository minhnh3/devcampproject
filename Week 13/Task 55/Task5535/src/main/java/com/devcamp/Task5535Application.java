package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5535Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5535Application.class, args);
	}

}
