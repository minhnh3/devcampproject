package com.devcamp.task5740;

public class CommissionEmployee extends Employee {
	private double grossSales;
	private double commissionRate;

	public CommissionEmployee() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CommissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales,
			double commissionRate) {
		super(firstName, lastName, socialSecurityNumber);
		// TODO Auto-generated constructor stub
		this.grossSales = grossSales;
		this.commissionRate = commissionRate;

	}

	public double getGrossSales() {
		return grossSales;
	}

	public void setGrossSales(double grossSales) {
		this.grossSales = grossSales;
	}

	public double getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(double commissionRate) {
		this.commissionRate = commissionRate;
	}

	@Override
	public double getPaymentAmount() {
		// TODO Auto-generated method stub
		return this.commissionRate * this.grossSales;
	}
}
