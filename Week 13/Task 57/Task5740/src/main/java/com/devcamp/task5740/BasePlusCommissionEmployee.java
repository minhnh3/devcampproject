package com.devcamp.task5740;

public class BasePlusCommissionEmployee extends CommissionEmployee {
	private double baseSalary;

	public BasePlusCommissionEmployee() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BasePlusCommissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales,
			double commissionRate, double baseSalary) {
		super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);
		// TODO Auto-generated constructor stub
		this.baseSalary = baseSalary;
	}

	public double getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}

	@Override
	public double getPaymentAmount() {
		// TODO Auto-generated method stub
		return this.getGrossSales() * this.getCommissionRate() + this.baseSalary;
	}
}
