package com.devcamp.task5740;

public interface Payable {
	public double getPaymentAmount();
}
