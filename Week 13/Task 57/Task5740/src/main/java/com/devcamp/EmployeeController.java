package com.devcamp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5740.BasePlusCommissionEmployee;
import com.devcamp.task5740.CommissionEmployee;
import com.devcamp.task5740.Employee;
import com.devcamp.task5740.HourlyEmployee;
import com.devcamp.task5740.Invoice;
import com.devcamp.task5740.SalariedEmployee;

@RestController
public class EmployeeController {
	@CrossOrigin
	@GetMapping("/listemployee")
	public List<Employee> getListEmployee() {
		Employee vCommissionEmployee = new CommissionEmployee("a", "A", "1000", 200000000, 0.5);
		Employee vHourlyEmployee = new HourlyEmployee("b", "B", "1001", 500000, 4);
		Employee vSalariedEmployee = new SalariedEmployee("c", "C", "1010", 700000);
		Employee vBasePlusCommissionEmployee = new BasePlusCommissionEmployee("z", "Z", "2000", 10000000, 0.5, 7000000);

		ArrayList<Employee> vListEmployee = new ArrayList<Employee>(
				Arrays.asList(vBasePlusCommissionEmployee, vCommissionEmployee));
		vListEmployee.add(vHourlyEmployee);
		vListEmployee.add(vSalariedEmployee);

		return vListEmployee;
	}

	@CrossOrigin
	@GetMapping("/listinvoice")
	public List<Invoice> getListInvoice() {
		Invoice v001 = new Invoice("001", "customer party", 2, 1000000);
		Invoice v002 = new Invoice("002", "transportation", 10, 100000);
		Invoice v003 = new Invoice("003", "employee birthday", 1, 1000000);
		ArrayList<Invoice> vListInvoice = new ArrayList<Invoice>(Arrays.asList(v003, v002, v001));
		return vListInvoice;
	}
}
