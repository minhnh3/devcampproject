package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5740Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5740Application.class, args);
	}

}
