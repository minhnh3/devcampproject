package com.devcamp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5720.Address;
import com.devcamp.task5720.CDPlayer;
import com.devcamp.task5720.DVDPlayer;
import com.devcamp.task5720.Person;
import com.devcamp.task5720.Player;
import com.devcamp.task5720.TapePlayer;

@RestController
public class PersonController {
	private final int CDPLAYER = 1;
	private final int DVDPLAYER = 2;
	private final int TAPEPLAYER = 3;

	@CrossOrigin
	@GetMapping("/personlist")
	public List<Person> getPerson(@RequestParam(value = "play", defaultValue = "0") String paramPlay) {
		ArrayList<Person> vPersonList = new ArrayList<Person>();
		Person vPersonMinh = new Person(30, "Minh", "male", new Address("Kim Ngưu", "Hà Nội", "Việt Nam", 100000),
				new ArrayList<Player>() {
					{
						add(new DVDPlayer());
						add(new CDPlayer());
					}
				}) {
			@Override
			public void eat() {
				// TODO Auto-generated method stub
			}
		};

		ArrayList<Player> vPersonNamPlayerList = new ArrayList<Player>();
		Player vTapePlayer = new TapePlayer();
		vPersonNamPlayerList.add(vTapePlayer);
		Address vPersonNamAddress = new Address("Hoàng Cầu", "Hà Nội", "Việt Nam", 100000);
		Person vPersonNam = new Person(40, "Nam", "male", vPersonNamAddress, vPersonNamPlayerList) {
			@Override
			public void eat() {
				// TODO Auto-generated method stub
			}
		};
		vPersonList.add(vPersonMinh);
		vPersonList.add(vPersonNam);

		ArrayList<Person> vPersonListFilter = new ArrayList<Person>();
		int vPlayType = Integer.parseInt(paramPlay);
		if (vPlayType == this.CDPLAYER || vPlayType == this.DVDPLAYER || vPlayType == this.TAPEPLAYER) {
			for (int i = 0; i < vPersonList.size(); i++) {
				ArrayList<Player> bPersonListPlay = vPersonList.get(i).getListPlay();
				for (Player bPlayerElement : bPersonListPlay) {
					if (vPlayType == this.CDPLAYER && bPlayerElement instanceof CDPlayer) {
						vPersonListFilter.add(vPersonList.get(i));
					}
					if (vPlayType == this.DVDPLAYER && bPlayerElement instanceof DVDPlayer) {
						vPersonListFilter.add(vPersonList.get(i));
					}
					if (vPlayType == this.TAPEPLAYER && bPlayerElement instanceof TapePlayer) {
						vPersonListFilter.add(vPersonList.get(i));
					}
				}
			}
		} else {
			vPersonListFilter = vPersonList;
		}
		return vPersonListFilter;
	}
}
