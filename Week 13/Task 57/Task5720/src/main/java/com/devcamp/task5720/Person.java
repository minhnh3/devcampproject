package com.devcamp.task5720;

import java.util.ArrayList;

public abstract class Person {
	private int age;
	private String name;
	private String gender;
	private Address address;
	private ArrayList<Player> listPlay;

	public Person() {
		super();
		// TODO Auto-generated constructor stub
		this.address = new Address();
	}

	public Person(int age, String name, String gender, Address address, ArrayList<Player> listPlay) {
		super();
		this.age = age;
		this.name = name;
		this.gender = gender;
		this.address = address;
		this.listPlay = listPlay;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ArrayList<Player> getListPlay() {
		return listPlay;
	}

	public void setListPlay(ArrayList<Player> listPlay) {
		this.listPlay = listPlay;
	}

	public abstract void eat();
}
