package com.devcamp.task5720;

public interface Recorder extends Player {
	public String getRecord();
}
