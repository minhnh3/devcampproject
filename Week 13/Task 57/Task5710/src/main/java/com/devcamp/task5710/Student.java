package com.devcamp.task5710;

import java.util.ArrayList;

public class Student extends Person implements ISchool {
	private int studentId;
	private ArrayList<Subject> listSubject;

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(int age, String gender, String name, Address address, ArrayList<Animals> listPet, int studentId,
			ArrayList<Subject> listSubject) {
		super(age, gender, name, address, listPet);
		// TODO Auto-generated constructor stub
		this.studentId = studentId;
		this.listSubject = listSubject;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public ArrayList<Subject> getListSubject() {
		return listSubject;
	}

	public void setListSubject(ArrayList<Subject> listSubject) {
		this.listSubject = listSubject;
	}

	public void addSubject(Subject subject) {
		this.listSubject.add(subject);
	}

	@Override
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Student sound");
	}

	@Override
	public void sleep() {
		// TODO Auto-generated method stub
		System.out.println("Student sound");
	}

	@Override
	public void gotoShop() {
		// TODO Auto-generated method stub
		System.out.println("Student sound");
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Student sound");
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Student eating");
	}

	@Override
	public void gotoShool() {
		// TODO Auto-generated method stub
		System.out.println("Student going to school");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Student:{age:" + this.getAge() + ", gender:" + this.getGender() + ", name:" + this.getName()
				+ ", address:" + this.getAddress() + ", listPet:" + this.getListPet() + ", studentId:" + this.studentId
				+ ", listSubject:" + this.listSubject + "}" + "\n";
	}

	public void doHomework() {
		System.out.println("Student doing homework");
	}
}
