package com.devcamp.task5710;

public class Duck extends Animals {
	private String beakColor;

	public Duck() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Duck(int age, String gender, String beakColor) {
		super(age, gender);
		// TODO Auto-generated constructor stub
		this.beakColor = beakColor;
	}

	public String getBeakColor() {
		return beakColor;
	}

	public void setBeakColor(String beakColor) {
		this.beakColor = beakColor;
	}

	@Override
	public boolean isMammal() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mate() {
		// TODO Auto-generated method stub
		System.out.println("Duck mate...");
	}

	@Override
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Duck sound");
	}

	@Override
	public void sleep() {
		// TODO Auto-generated method stub
		System.out.println("Duck sleep");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Duck{age:" + this.getAge() + ", gender:" + this.getGender() + ", beakColor:" + this.beakColor + "}";
	}

	public void swim() {
		System.out.println("Duck swimming");
	}

	public void quack() {
		System.out.println("Duck quacking");
	}
}
