package com.devcamp.task5710;

public class Fish extends Animals {
	private int size;
	private boolean canEat;

	public Fish() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Fish(int age, String gender, int size, boolean canEat) {
		super(age, gender);
		// TODO Auto-generated constructor stub
		this.size = size;
		this.canEat = canEat;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isCanEat() {
		return canEat;
	}

	public void setCanEat(boolean canEat) {
		this.canEat = canEat;
	}

	@Override
	public boolean isMammal() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mate() {
		// TODO Auto-generated method stub
		System.out.println("Fish mate...");
	}

	@Override
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Fish sound");
	}

	@Override
	public void sleep() {
		// TODO Auto-generated method stub
		System.out.println("Fish sleep");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Fish{age:" + this.getAge() + ", gender:" + this.getGender() + ", size:" + this.size + ", canEat:"
				+ this.canEat + "}";
	}

	public void swim() {
		System.out.println("Fish swimming");
	}
}
