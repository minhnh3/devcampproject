package com.devcamp.task5710;

import java.util.ArrayList;

public abstract class Person implements IAnimal, ILive {
	private int age;
	private String name;
	private String gender;
	private Address address;
	private ArrayList<Animals> listPet;

	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Person(int age, String name, String gender, Address address, ArrayList<Animals> listPet) {
		super();
		this.age = age;
		this.name = name;
		this.gender = gender;
		this.address = address;
		this.listPet = listPet;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ArrayList<Animals> getListPet() {
		return listPet;
	}

	public void setListPet(ArrayList<Animals> listPet) {
		this.listPet = listPet;
	}

	public abstract void eat();
}
