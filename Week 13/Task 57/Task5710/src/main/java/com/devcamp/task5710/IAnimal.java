package com.devcamp.task5710;

public interface IAnimal {
	public void animalSound();

	public void sleep();
}
