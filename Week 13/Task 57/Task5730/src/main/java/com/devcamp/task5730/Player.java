package com.devcamp.task5730;

public interface Player {
	public String getPlay();

	public String getStop();

	public String getPause();

	public String getReverse();
}
