package com.devcamp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5730.Address;
import com.devcamp.task5730.Ball;
import com.devcamp.task5730.Baseball;
import com.devcamp.task5730.CDPlayer;
import com.devcamp.task5730.DVDPlayer;
import com.devcamp.task5730.Football;
import com.devcamp.task5730.Person;
import com.devcamp.task5730.Player;
import com.devcamp.task5730.TapePlayer;

@RestController
public class PersonController {
	private final int BASEBALL = 1;
	private final int FOOTBALL = 2;

	@CrossOrigin
	@GetMapping("/listpeople")
	public List<Person> getListPeople(@RequestParam(value = "ballType", defaultValue = "0") String paramBallType) {
		Player vMinhCD01Player = new CDPlayer();
		Player vMinhCD02Player = new CDPlayer();
		Player vMinhTape01Player = new TapePlayer();
		Ball vMinhFootballVNBall = new Football("VN Football");
		Person vMinh = new Person(30, "Minh", "male", new Address("Kim Ngưu", "Hà Nội", "Việt Nam", 100000),
				new ArrayList<Player>(Arrays.asList(vMinhCD01Player, vMinhCD02Player, vMinhTape01Player)),
				new ArrayList<Ball>(Arrays.asList(vMinhFootballVNBall))) {
			@Override
			public void eat() {
				// TODO Auto-generated method stub
			}
		};
		Player vJayChouDVDNoWayPlayer = new DVDPlayer();
		Ball vJayChouBaseballTencentBall = new Baseball("Tencent Baseball");
		Ball vJayChouFootballAliBall = new Football("Ali Football");
		Person vJayChou = new Person(55, "JayChou", "male", new Address("Tam Trinh", "Đà Nẵng", "Việt Nam", 100000),
				new ArrayList<Player>() {
					{
						add(vJayChouDVDNoWayPlayer);
					}
				}, new ArrayList<Ball>(Arrays.asList(vJayChouBaseballTencentBall, vJayChouFootballAliBall))) {
			@Override
			public void eat() {
				// TODO Auto-generated method stub
			}
		};
		Ball vKimBaseballNYBall = new Baseball("NY Baseball");
		Person vKim = new Person(40, "Kim", "male", new Address("New Square", "New York", "USA", 200000),
				new ArrayList<Player>(), new ArrayList<Ball>(Arrays.asList(vKimBaseballNYBall))) {
			@Override
			public void eat() {
				// TODO Auto-generated method stub
			}
		};

		ArrayList<Person> vListPeople = new ArrayList<Person>(Arrays.asList(vJayChou, vMinh));
		vListPeople.add(vKim);
		ArrayList<Person> vFilterListPeople = new ArrayList<Person>();
		int vBallType = Integer.parseInt(paramBallType);
		if (vBallType != BASEBALL && vBallType != FOOTBALL) {
			vFilterListPeople = vListPeople;
		} else {
			for (Iterator<Person> iterator = vListPeople.iterator(); iterator.hasNext();) {
				Person person = (Person) iterator.next();
				ArrayList<Ball> bListBall = person.getListBall();
				for (Iterator<Ball> iterator2 = bListBall.iterator(); iterator2.hasNext();) {
					Ball vBall = (Ball) iterator2.next();
					if (vBallType == BASEBALL && vBall instanceof Baseball) {
						vFilterListPeople.add(person);
					}
					if (vBallType == FOOTBALL && vBall instanceof Football) {
						vFilterListPeople.add(person);
					}
				}
			}
		}
		return vFilterListPeople;
	}
}
