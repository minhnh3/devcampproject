package com.devcamp.task5730;

public abstract class Ball implements Tossable {
	private String brandName;

	public Ball() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Ball(String brandName) {
		super();
		this.setBrandName(brandName);
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public abstract void bounce();

	@Override
	public abstract void toss();

}
