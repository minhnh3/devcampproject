package com.devcamp.task5730;

public interface Recorder extends Player {
	public String getRecord();
}
