package com.devcamp.task5730;

public interface Tossable {
	public void toss();
}
