package com.devcamp;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58s50.Album;
import com.devcamp.task58s50.Image;
import com.devcamp.task58s50.Library;

@RestController
public class LibraryController {
	@CrossOrigin
	@GetMapping("/library")
	public Library getLibrary() {
		Image vSongokuImage01 = new Image("I01", "Songoku 01", "https://www.dragonball.db/songoku01",
				"Songoku Image 01", "22/22/2222");
		Image vSongokuImage02 = new Image("I02", "Songoku 02", "https://www.dragonball.db/songoku02",
				"Songoku Image 02 ", "02/02/2020");
		Album vAlbumSongoku = new Album("A01", "Album Songoku", "All Songoku Images", "11/11/1111",
				new ArrayList<Image>(Arrays.asList(vSongokuImage01, vSongokuImage02)));
		Image vVegetaImage01 = new Image("I04", "Vegeta 01", "https://www.dragonball.db/vegeta01", "Vegeta Image 01 ",
				"03/03/3030");
		Album vAlbumVegeta = new Album("A02", "Album Vegeta", "All Vegeta Images", "33/33/3333",
				new ArrayList<Image>(Arrays.asList(vVegetaImage01)));
		Library vImageLibrary = new Library(new ArrayList<Album>(Arrays.asList(vAlbumSongoku, vAlbumVegeta)));
		return vImageLibrary;
	}
}
