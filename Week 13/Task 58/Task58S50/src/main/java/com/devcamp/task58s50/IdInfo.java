package com.devcamp.task58s50;

public abstract class IdInfo {
	private String code;
	private String name;
	private String description;
	private String createdDate;

	public IdInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IdInfo(String code, String name, String description, String createdDate) {
		super();
		this.code = code;
		this.name = name;
		this.description = description;
		this.createdDate = createdDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

}
