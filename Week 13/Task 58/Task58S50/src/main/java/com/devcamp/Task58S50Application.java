package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task58S50Application {

	public static void main(String[] args) {
		SpringApplication.run(Task58S50Application.class, args);
	}

}
