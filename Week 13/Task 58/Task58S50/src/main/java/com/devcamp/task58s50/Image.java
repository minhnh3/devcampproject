package com.devcamp.task58s50;

public class Image extends IdInfo {
	private String link;

	public Image() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Image(String code, String name, String link, String description, String createdDate) {
		super(code, name, description, createdDate);
		// TODO Auto-generated constructor stub
		this.link = link;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
