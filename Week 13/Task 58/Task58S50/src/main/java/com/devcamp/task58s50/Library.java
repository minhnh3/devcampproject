package com.devcamp.task58s50;

import java.util.ArrayList;

public class Library {
	private ArrayList<Album> listAlbum;

	public Library() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Library(ArrayList<Album> listAlbum) {
		super();
		this.listAlbum = listAlbum;
	}

	public ArrayList<Album> getListAlbum() {
		return listAlbum;
	}

	public void setListAlbum(ArrayList<Album> listAlbum) {
		this.listAlbum = listAlbum;
	}

}
