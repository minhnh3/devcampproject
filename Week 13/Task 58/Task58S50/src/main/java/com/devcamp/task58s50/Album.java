package com.devcamp.task58s50;

import java.util.ArrayList;

public class Album extends IdInfo {
	private ArrayList<Image> listImage;

	public Album() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Album(String code, String name, String description, String createdDate, ArrayList<Image> listImage) {
		super(code, name, description, createdDate);
		// TODO Auto-generated constructor stub
		this.listImage = listImage;
	}

	public ArrayList<Image> getListImage() {
		return listImage;
	}

	public void setListImage(ArrayList<Image> listImage) {
		this.listImage = listImage;
	}

}
