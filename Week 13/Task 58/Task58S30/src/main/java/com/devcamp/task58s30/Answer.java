package com.devcamp.task58s30;

public class Answer extends IdInfo {
	private String result;
	private String explaination;

	public Answer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Answer(String code, String content, String result, String explaination) {
		super(code, content);
		// TODO Auto-generated constructor stub
		this.result = result;
		this.explaination = explaination;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getExplain() {
		return explaination;
	}

	public void setExplain(String explain) {
		this.explaination = explain;
	}

}
