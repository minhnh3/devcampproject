package com.devcamp;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58s30.Answer;
import com.devcamp.task58s30.Exam;
import com.devcamp.task58s30.Question;

@RestController
public class ExamController {
	@CrossOrigin
	@GetMapping("/exam")
	public Exam getExam() {
		Question vQ1 = new Question("Q1", "Java is used for backend", 4, "Java", "11/11/1111",
				new Answer("AQ1", "Java is usually for backend", "A", "search ggl definition"));
		Question vQ2 = new Question("Q2", "Java is not OOP", 3, "Java", "22/22/2222",
				new Answer("AQ2", "Java is OOP", "B", "search ggl"));
		Question vQ3 = new Question("Q3", "Java is used for frontend", 3, "Java", "33/33/3333",
				new Answer("AQ3", "Java can be used for frontend", "D", "search ggl"));
		Exam vJava01 = new Exam(new ArrayList<Question>(Arrays.asList(vQ1, vQ2, vQ3)));
		return vJava01;
	}
}
