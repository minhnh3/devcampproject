package com.devcamp.task58s30;

import java.util.ArrayList;

public class Exam {
	private ArrayList<Question> listQuestion;

	public Exam() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Exam(ArrayList<Question> listQuestion) {
		super();
		this.listQuestion = listQuestion;
	}

	public ArrayList<Question> getListQuestion() {
		return listQuestion;
	}

	public void setListQuestion(ArrayList<Question> listQuestion) {
		this.listQuestion = listQuestion;
	}
}
