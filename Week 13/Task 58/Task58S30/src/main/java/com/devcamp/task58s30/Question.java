package com.devcamp.task58s30;

public class Question extends IdInfo {
	private int point;
	private String subject;
	private String createdDate;
	private Answer answer;

	public Question() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Question(String code, String content, int point, String subject, String createdDate, Answer answer) {
		super(code, content);
		// TODO Auto-generated constructor stub
		this.point = point;
		this.subject = subject;
		this.createdDate = createdDate;
		this.answer = answer;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

}
