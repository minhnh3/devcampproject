package com.devcamp.task58s30;

public abstract class IdInfo {
	private String code;
	private String content;

	public IdInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IdInfo(String code, String content) {
		super();
		this.code = code;
		this.content = content;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
