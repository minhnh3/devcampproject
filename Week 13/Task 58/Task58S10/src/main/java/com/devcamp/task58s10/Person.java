package com.devcamp.task58s10;

public abstract class Person {
	private String name;
	private String phoneNumber;

	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Person(String name, String phoneNumber) {
		super();
		this.name = name;
		this.phoneNumber = phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
