package com.devcamp.task58s10;

import java.util.ArrayList;

public class School {
	private ArrayList<Classroom> listClass;

	public School() {
		super();
		// TODO Auto-generated constructor stub
	}

	public School(ArrayList<Classroom> listClass) {
		super();
		this.listClass = listClass;
	}

	public ArrayList<Classroom> getListClass() {
		return listClass;
	}

	public void setListClass(ArrayList<Classroom> listClass) {
		this.listClass = listClass;
	}
}
