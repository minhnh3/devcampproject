package com.devcamp.task58s10;

public class Student extends Person {
	private String studentCode;
	private String studentGender;
	private String studentBirthday;
	private String studentAddress;

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(String studentCode, String name, String studentGender, String studentBirthday, String studentAddress,
			String phoneNumber) {
		super(name, phoneNumber);
		// TODO Auto-generated constructor stub
		this.studentCode = studentCode;
		this.studentGender = studentGender;
		this.studentBirthday = studentBirthday;
		this.studentAddress = studentAddress;
	}

	public String getStudentCode() {
		return studentCode;
	}

	public void setStudentCode(String studentCode) {
		this.studentCode = studentCode;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public String getStudentBirthday() {
		return studentBirthday;
	}

	public void setStudentBirthday(String studentBirthday) {
		this.studentBirthday = studentBirthday;
	}

	public String getStudentAddress() {
		return studentAddress;
	}

	public void setStudentAddress(String studentAddress) {
		this.studentAddress = studentAddress;
	}

}
