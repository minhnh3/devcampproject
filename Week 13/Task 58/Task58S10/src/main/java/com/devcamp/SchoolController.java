package com.devcamp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58s10.Classroom;
import com.devcamp.task58s10.School;
import com.devcamp.task58s10.Student;
import com.devcamp.task58s10.Teacher;

@RestController
public class SchoolController {
	@CrossOrigin
	@GetMapping("/school")
	public School getSchool() {
		Student va = new Student("10a", "a", "male", "01/01/1000", "Hà Nội - Việt Nam", "1234567");
		Student vb = new Student("10b", "b", "female", "02/02/1002", "Hồ Chí Minh - Việt Nam", "2314567");
		Classroom v10A = new Classroom("10A", "10 - A", new Teacher("Minh", "0123"),
				new ArrayList<Student>(Arrays.asList(va, vb)));
		Classroom v11B = new Classroom("11B", "11 - B", new Teacher("Kim", "4523"),
				new ArrayList<Student>(Arrays.asList()));
		Student vd = new Student("10d", "d", "female", "11/11/1111", "Hà Nội - Việt Nam", "3456741");
		Classroom v12Z = new Classroom("12Z", "12 - Z", new Teacher("Joe", "9865"),
				new ArrayList<Student>(Arrays.asList(vd)));
		School vTranPhu = new School(new ArrayList<Classroom>(Arrays.asList(v10A, v11B, v12Z)));

		return vTranPhu;
	}
}
