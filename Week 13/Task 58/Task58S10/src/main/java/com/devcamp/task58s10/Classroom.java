package com.devcamp.task58s10;

import java.util.ArrayList;

public class Classroom {
	private String classroomCode;
	private String classroomName;
	private Teacher mainTeacher;
	private ArrayList<Student> listStudent;

	public Classroom() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Classroom(String classroomCode, String classroomName, Teacher mainTeacher, ArrayList<Student> listStudent) {
		super();
		this.classroomCode = classroomCode;
		this.classroomName = classroomName;
		this.mainTeacher = mainTeacher;
		this.listStudent = listStudent;
	}

	public String getClassroomCode() {
		return classroomCode;
	}

	public void setClassroomCode(String classroomCode) {
		this.classroomCode = classroomCode;
	}

	public String getClassroomName() {
		return classroomName;
	}

	public void setClassroomName(String classroomName) {
		this.classroomName = classroomName;
	}

	public Teacher getMainTeacher() {
		return mainTeacher;
	}

	public void setMainTeacher(Teacher mainTeacher) {
		this.mainTeacher = mainTeacher;
	}

	public ArrayList<Student> getListStudent() {
		return listStudent;
	}

	public void setListStudent(ArrayList<Student> listStudent) {
		this.listStudent = listStudent;
	}
}
