package com.devcamp.task58s20;

import java.util.ArrayList;

public class Company {
	private ArrayList<Department> listDepartment;

	public Company() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Company(ArrayList<Department> listDepartment) {
		super();
		this.listDepartment = listDepartment;
	}

	public ArrayList<Department> getListDepartment() {
		return listDepartment;
	}

	public void setListDepartment(ArrayList<Department> listDepartment) {
		this.listDepartment = listDepartment;
	}
}
