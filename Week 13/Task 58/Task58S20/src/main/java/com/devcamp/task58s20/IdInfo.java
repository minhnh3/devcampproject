package com.devcamp.task58s20;

public abstract class IdInfo {
	private String code;
	private String name;

	public IdInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IdInfo(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
