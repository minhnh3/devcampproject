package com.devcamp.task58s20;

public class Staff extends IdInfo {
	private String position;
	private String gender;
	private String dateOfBirth;
	private String address;
	private String phoneNumber;

	public Staff() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Staff(String code, String name, String position, String gender, String dateOfBirth, String address,
			String phoneNumber) {
		super(code, name);
		// TODO Auto-generated constructor stub
		this.position = position;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
