package com.devcamp.task58s20;

import java.util.ArrayList;

public class Department extends IdInfo {
	private String majorBusiness;
	private String intro;
	private ArrayList<Staff> listStaff;

	public Department() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Department(String code, String name, String majorBusiness, String intro, ArrayList<Staff> listStaff) {
		super(code, name);
		// TODO Auto-generated constructor stub
		this.majorBusiness = majorBusiness;
		this.intro = intro;
		this.listStaff = listStaff;
	}

	public String getMajorBusiness() {
		return majorBusiness;
	}

	public void setMajorBusiness(String majorBusiness) {
		this.majorBusiness = majorBusiness;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public ArrayList<Staff> getListStaff() {
		return listStaff;
	}

	public void setListStaff(ArrayList<Staff> listStaff) {
		this.listStaff = listStaff;
	}
}
