package com.devcamp;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58s20.Company;
import com.devcamp.task58s20.Department;
import com.devcamp.task58s20.Staff;

@RestController
public class CompanyController {
	@CrossOrigin
	@GetMapping("/company")
	public Company getCompany() {
		Department vMarketingABC = new Department("01", "Marketing", "marketing-communication-advertising",
				"key of ABC company", new ArrayList<Staff>(Arrays.asList()));

		Staff vMinh = new Staff("s01", "Minh", "Sale Manager", "Male", "08/07/1992", "Hà Nội", "1234567");
		Department vSaleABC = new Department("02", "Sale", "sale product", "make money for ABC company",
				new ArrayList<Staff>(Arrays.asList(vMinh)));

		Staff vA = new Staff("s02", "A", "Production Manager", "Female", "11/11/2000", "Hà Nội", "4567012");
		Staff vB = new Staff("s03", "B", "Production Staff", "Female", "12/12/2002", "Hà Nội", "2135567");
		Department vProductionABC = new Department("03", "Production", "produce goods", "soul for ABC company",
				new ArrayList<Staff>(Arrays.asList(vA, vB)));
		Company vABC = new Company(new ArrayList<Department>(Arrays.asList(vMarketingABC, vSaleABC, vProductionABC)));
		return vABC;
	}
}
