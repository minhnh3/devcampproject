package com.devcamp.task58s40;

public abstract class IdInfo {
	private String code;
	private String name;
	private String intro;
	private long page;

	public IdInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IdInfo(String code, String name, String intro, long page) {
		super();
		this.code = code;
		this.name = name;
		this.intro = intro;
		this.page = page;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public long getPage() {
		return page;
	}

	public void setPage(long page) {
		this.page = page;
	}

}
