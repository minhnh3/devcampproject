package com.devcamp;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58s40.Book;
import com.devcamp.task58s40.Chapter;
import com.devcamp.task58s40.Lesson;

@RestController
public class BookController {
	@CrossOrigin
	@GetMapping("/book")
	public Book getBook() {
		Lesson vLesson01 = new Lesson("L01", "Starting for Java", "First steps for Java", 2);
		Chapter vChapter01 = new Chapter("C01", "Java for newbie", "All messages for newbie", "Minh", 1,
				new ArrayList<Lesson>(Arrays.asList(vLesson01)));

		Lesson vLesson02 = new Lesson("L02", "Variable - Data Types", "Basic definitions", 4);
		Lesson vLesson03 = new Lesson("L03", "Methods", "The way to implement idea", 7);
		Chapter vChapter02 = new Chapter("C02", "Java basic", "Show you all foundation of Java", "Nam", 3,
				new ArrayList<Lesson>(Arrays.asList(vLesson02, vLesson03)));

		Lesson vLesson04 = new Lesson("L04", "OOP", "The way to implement idea", 9);
		Chapter vChapter03 = new Chapter("C03", "Java advanced", "How to perfect", "John", 8,
				new ArrayList<Lesson>(Arrays.asList(vLesson04)));
		Book vJavaBasic = new Book(new ArrayList<Chapter>(Arrays.asList(vChapter01, vChapter02, vChapter03)));
		return vJavaBasic;
	}
}
