package com.devcamp.task58s40;

import java.util.ArrayList;

public class Book {
	private ArrayList<Chapter> listChapter;

	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Book(ArrayList<Chapter> listChapter) {
		super();
		this.listChapter = listChapter;
	}

	public ArrayList<Chapter> getListChapter() {
		return listChapter;
	}

	public void setListChapter(ArrayList<Chapter> listChapter) {
		this.listChapter = listChapter;
	}

}
