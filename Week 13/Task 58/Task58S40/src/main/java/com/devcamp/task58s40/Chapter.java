package com.devcamp.task58s40;

import java.util.ArrayList;

public class Chapter extends IdInfo {
	private String translatorName;
	private ArrayList<Lesson> listLesson;

	public Chapter() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Chapter(String code, String name, String intro, String translatorName, long page,
			ArrayList<Lesson> listLesson) {
		super(code, name, intro, page);
		// TODO Auto-generated constructor stub
		this.translatorName = translatorName;
		this.listLesson = listLesson;
	}

	public String getTranslatorName() {
		return translatorName;
	}

	public void setTranslatorName(String translatorName) {
		this.translatorName = translatorName;
	}

	public ArrayList<Lesson> getListLesson() {
		return listLesson;
	}

	public void setListLesson(ArrayList<Lesson> listLesson) {
		this.listLesson = listLesson;
	}

}
