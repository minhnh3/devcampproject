package com.devcamp.task5160;

import com.devcamp.CCat;
import com.devcamp.CDog;
import com.devcamp.petpkg.CPet;

public class CMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CPet vPet = new CPet(1, "X");
		vPet.bark();
		vPet.print();
		vPet.play();
		System.out.println("---");
		vPet = new CCat(2, "catcat");
		vPet.bark();
		vPet.print();
		vPet.play();
		System.out.println("---");
		vPet = new CDog(3, "dogdog");
		vPet.bark();
		vPet.print();
		vPet.play();

		System.out.println("===");
		CPet vCatPet = new CCat(22, "catcatcatcat");
		vCatPet.bark();
		vCatPet.print();
		vCatPet.play();

		System.out.println("===");
		CPerson vMinh = new CPerson(007, 30, "Minh", "Nguyen", "VN");
		vMinh.printPerson();
		System.out.println("---");
		vMinh.addPet(vPet);
		vMinh.addPet(vCatPet);
		vMinh.printPerson();
	}

}
