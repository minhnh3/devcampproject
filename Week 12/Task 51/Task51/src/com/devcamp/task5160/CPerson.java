package com.devcamp.task5160;

import java.util.ArrayList;

import com.devcamp.petpkg.CPet;

public class CPerson {
	private int id;
	private int age;
	private String firstName;
	private String lastName;
	private ArrayList<CPet> pets;
	private String country;

	public CPerson() {
		super();
		this.pets = new ArrayList<CPet>();
	}

	public CPerson(int id, int age, String firstName, String lastName, String country) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.pets = new ArrayList<CPet>();
		this.country = country;
	}

	public CPerson(int id, int age, String firstName, String lastName, ArrayList<CPet> pets, String country) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.pets = pets;
		this.country = country;
	}

	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	public void addPet(CPet paramPet) {
		this.pets.add(paramPet);
	}

	private void printAllPets() {
		System.out.println("Pets List: ");
		for (CPet cPet : pets) {
			cPet.print();
		}
	};

	public void printPerson() {
		System.out.println("id = " + this.id);
		System.out.println("age = " + this.age);
		System.out.println("firstName = " + this.firstName);
		System.out.println("lastName = " + this.lastName);
		System.out.println("fullname = " + this.getFullName());
		System.out.println("pets = ");
		System.out.println(this.pets);
		printAllPets();
		System.out.println(this.country);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ArrayList<CPet> getPets() {
		return pets;
	}

	public void setPets(ArrayList<CPet> pets) {
		this.pets = pets;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
