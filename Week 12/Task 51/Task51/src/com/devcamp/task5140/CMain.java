package com.devcamp.task5140;

import com.devcamp.task5110.CCar;

public class CMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CCar vAudi = new CCar("Audi", 2222, "A", "A-2");
		CCar vVinfast = new CCar("Vinfast", 3333, "S", "S-3");
		System.out.println("Max speed: " + String.valueOf(vAudi.maxSpeedPerKm()));
		System.out.println("Max speed: " + vVinfast.maxSpeedPerKm());
		System.out.println("---");
		CPerson vMinh = new CPerson(1, 30, "Minh", "Nguyen", "VN");
		vMinh.printPerson();
		vMinh.addCar(vAudi);
		vMinh.addCar(vVinfast);
		System.out.println("---");
		vMinh.printPerson();

	}

}
