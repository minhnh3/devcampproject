package com.devcamp.task5140;

import java.util.ArrayList;

import com.devcamp.task5110.CCar;

public class CPerson {
	private int id;
	private int age;
	private String firstName;
	private String lastName;
	private ArrayList<CCar> car;
	private String country;

	public CPerson() {
		super();
		this.car = new ArrayList<CCar>();
	}

	public CPerson(int id, int age, String firstName, String lastName, String country) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.car = new ArrayList<CCar>();
		this.country = country;
	}

	public CPerson(int id, int age, String firstName, String lastName, ArrayList<CCar> car, String country) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.car = car;
		this.country = country;
	}

	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	public void addCar(CCar paramCar) {
		this.car.add(paramCar);
	}

	private void printAllCars() {
		System.out.println("Car List: ");
		for (CCar cCar : car) {
			System.out.println("- " + cCar.getBrand() + ":");
			System.out.println("+ yearManufactured = " + cCar.getYearManufactured());
			System.out.println("+ modelName = " + cCar.getModelName());
			System.out.println("+ vid = " + cCar.getVid());
		}
	};

	public void printPerson() {
		System.out.println("id = " + this.id);
		System.out.println("age = " + this.age);
		System.out.println("firstName = " + this.firstName);
		System.out.println("lastName = " + this.lastName);
		System.out.println("car = ");
		System.out.println(this.car);
		printAllCars();
		System.out.println(this.country);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ArrayList<CCar> getCar() {
		return car;
	}

	public void setCar(ArrayList<CCar> car) {
		this.car = car;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
