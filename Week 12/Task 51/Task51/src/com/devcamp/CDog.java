package com.devcamp;

import com.devcamp.petpkg.CPet;

public class CDog extends CPet {
	@Override
	public void bark() {
		// TODO Auto-generated method stub
		System.out.println("gau gau...");
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		System.out.println("Thông tin Dog:");
		System.out.println("Dog age = " + this.age);
		System.out.println("Dog name = " + this.name);
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("gauing...");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{age:" + this.age + ", name:" + this.name + "}";
	}

	public CDog() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CDog(int age, String name) {
		super(age, name);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		CDog vDog = new CDog(3, "gau");
		vDog.bark();
		vDog.play();
		vDog.print();
		System.out.println("===");
		CPet vDogPet = new CDog(33, "gaow");
		vDogPet.bark();
		vDogPet.play();
		vDogPet.print();
	}
}
