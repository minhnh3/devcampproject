package com.devcamp.task5150;

import com.devcamp.task5110.CCar;
import com.devcamp.task5110.CMotorbike;
import com.devcamp.task5110.CVehicle;

public class CMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CVehicle vAudi = new CVehicle("Audi", 2222);
		vAudi.print();
		vAudi.honk();
		System.out.println("Max speed: " + String.valueOf(vAudi.maxSpeedPerKm()));

		System.out.println("---");
		vAudi = new CCar("Audi", 2222, "A", "A-2");
		vAudi.print();
		vAudi.honk();
		System.out.println("Max speed: " + vAudi.maxSpeedPerKm());

		System.out.println("---");
		CVehicle vHonda = new CMotorbike("Honda", 3333, "Dream", "D-3");
		vHonda.print();
		vHonda.honk();
		System.out.println("Max speed: " + vHonda.maxSpeedPerKm());

		System.out.println("---");
		CPerson vMinh = new CPerson(1, 30, "Minh", "Nguyen", "VN");
		vMinh.printPerson();
		System.out.println("---");
		vMinh.addVehicle(vAudi);
		vMinh.addVehicle(vHonda);
		vMinh.printPerson();

	}

}
