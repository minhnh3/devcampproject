package com.devcamp.task5150;

import java.util.ArrayList;

import com.devcamp.task5110.CVehicle;

public class CPerson {
	private int id;
	private int age;
	private String firstName;
	private String lastName;
	private ArrayList<CVehicle> vehicles;
	private String country;

	public CPerson() {
		super();
		this.vehicles = new ArrayList<CVehicle>();
	}

	public CPerson(int id, int age, String firstName, String lastName, String country) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.vehicles = new ArrayList<CVehicle>();
		this.country = country;
	}

	public CPerson(int id, int age, String firstName, String lastName, ArrayList<CVehicle> car, String country) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.vehicles = car;
		this.country = country;
	}

	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	public void addVehicle(CVehicle paramVehicle) {
		this.vehicles.add(paramVehicle);
	}

	private void printAllVehicles() {
		System.out.println("Vehicles List: ");
		for (CVehicle cVehicle : vehicles) {
			cVehicle.print();
		}

	};

	public void printPerson() {
		System.out.println("id = " + this.id);
		System.out.println("age = " + this.age);
		System.out.println("firstName = " + this.firstName);
		System.out.println("lastName = " + this.lastName);
		System.out.println("fullname = " + this.getFullName());
		System.out.println("vehicles = ");
		System.out.println(this.vehicles);
		printAllVehicles();
		System.out.println(this.country);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ArrayList<CVehicle> getCar() {
		return vehicles;
	}

	public void setCar(ArrayList<CVehicle> car) {
		this.vehicles = car;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
