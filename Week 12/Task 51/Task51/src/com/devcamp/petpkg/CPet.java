package com.devcamp.petpkg;

public class CPet {
	protected int age;
	protected String name;

	public void bark() {
		System.out.println("barking...");
	}

	public void print() {
		System.out.println("Thông tin Pet:");
		System.out.println("age = " + this.age);
		System.out.println("name = " + this.name);
	}

	public void play() {
		System.out.println("playing...");
	}

	public CPet() {
	}

	public CPet(int age, String name) {
		this.age = age;
		this.name = name;
	}

}
