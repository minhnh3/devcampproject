package com.devcamp.task5110;

public class CCar extends CVehicle {
	private String modelName;
	private String vid;

	//
	public CCar() {
	}

	public CCar(String paramBrand, int paramYearManufactured) {
		super(paramBrand, paramYearManufactured);
		// TODO Auto-generated constructor stub
	}

	//
	public CCar(String paramBrand, int paramYearManufactured, String paramModelName, String paramVid) {
		super(paramBrand, paramYearManufactured);
		modelName = paramModelName;
		vid = paramVid;
	}

	@Override
	public int maxSpeedPerKm() {
		// TODO Auto-generated method stub
		return 1000;
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		System.out.println("- " + this.brand + " information:");
		System.out.println("+ yearManufactured = " + this.yearManufactured);
		System.out.println("+ modelName = " + this.modelName);
		System.out.println("+ vid = " + this.vid);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{brand:" + this.brand + ", yearManufactured:" + this.yearManufactured + ", modelName:" + this.modelName
				+ ", vid:" + this.vid + "}";
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getYearManufactured() {
		return this.yearManufactured;
	}

	public void setYearManufactured(int yearManufactured) {
		this.yearManufactured = yearManufactured;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getVid() {
		return vid;
	}

	public void setVid(String vid) {
		this.vid = vid;
	}

	public static void main(String[] args) {
		CCar myFastCar = new CCar();
		myFastCar.honk();
		System.out.println(myFastCar.brand + " " + myFastCar.modelName);
		System.out.println("Max speed: " + String.valueOf(myFastCar.maxSpeedPerKm()));
		System.out.println("===");
	}
}