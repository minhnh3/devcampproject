package com.devcamp.task5110;

public class CVehicle {
	// sử dụng được ở class con (subclass)
	protected String brand;
	protected int yearManufactured;

	//
	public CVehicle() {
	}

	//
	public CVehicle(String paramBrand, int paramYearManufactured) {
		brand = paramBrand;
		yearManufactured = paramYearManufactured;
	}

	// max
	public void print() {
		System.out.println("CVehicle information... ");
		System.out.println("brand: " + brand);
		System.out.println("year manufactured: " + yearManufactured);
	}

	// max speed of vehicle
	public int maxSpeedPerKm() {
		return 0;
	};

	//
	public void honk() {
		System.out.println("Tu, tu!");
	}
}
