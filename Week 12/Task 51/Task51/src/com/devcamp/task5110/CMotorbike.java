package com.devcamp.task5110;

public class CMotorbike extends CVehicle {
	private String modelName;
	private String vid;

	public CMotorbike() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CMotorbike(String paramBrand, int paramYearManufactured, String paramModelName, String paramVid) {
		super(paramBrand, paramYearManufactured);
		// TODO Auto-generated constructor stub
		this.modelName = paramModelName;
		this.vid = paramVid;
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		super.print();
		System.out.println("modelName: " + modelName);
		System.out.println("vid: " + vid);
	}

	@Override
	public int maxSpeedPerKm() {
		// TODO Auto-generated method stub
		return 3000;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{brand:" + this.brand + ", yearManufactured:" + this.yearManufactured + ", modelName:" + this.modelName
				+ ", vid:" + this.vid + "}";
	}

	public void methodOfMotorbike() {
		System.out.println("Method Of Motorbike");
	}

	public static void main(String[] args) {
		CMotorbike myMotorbike = new CMotorbike("Honda", 2222, "Dream", "d-1992");
		myMotorbike.print();
		System.out.println("Max speed: " + myMotorbike.maxSpeedPerKm());
		myMotorbike.honk();
		System.out.println("===");
		CVehicle myMotorbikeVehicle = new CMotorbike("Suzuki", 3333, "Viva", "v-1992");
//		myMotorbikeVehicle.modelName = "x"; // KHÔNG TRUY XUẤT ĐƯỢC
//		myMotorbikeVehicle.vid = "x"; // KHÔNG TRUY XUẤT ĐƯỢC
		myMotorbikeVehicle.print();
		System.out.println("Max speed: " + myMotorbike.maxSpeedPerKm());
		myMotorbikeVehicle.honk();
//		myMotorbikeVehicle.methodOfMotorbike(); // KHÔNG TRUY XUẤT ĐƯỢC
	}
}
