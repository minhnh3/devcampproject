package com.devcamp;

import com.devcamp.petpkg.CPet;

public class CCat extends CPet {
	@Override
	public void bark() {
		// TODO Auto-generated method stub
		System.out.println("meo meo...");
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		System.out.println("Thông tin Cat:");
		System.out.println("Cat age = " + this.age);
		System.out.println("Cat name = " + this.name);
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("sleeping...");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{age:" + this.age + ", name:" + this.name + "}";
	}

	public CCat() {
		// TODO Auto-generated constructor stub
	}

	public CCat(int age, String name) {
		super(age, name);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		CCat vCat = new CCat(2, "meo");
		vCat.bark();
		vCat.play();
		vCat.print();
		System.out.println("===");
		CPet vCatPet = new CCat(22, "miao");
		vCatPet.bark();
		vCatPet.play();
		vCatPet.print();
	}
}
