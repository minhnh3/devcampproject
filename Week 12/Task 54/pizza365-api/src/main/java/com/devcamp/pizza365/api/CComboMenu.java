package com.devcamp.pizza365.api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.api.menu.CMenu;

//@RestController
public class CComboMenu {
//	@CrossOrigin
//	@GetMapping
	public ArrayList<CMenu> getListMenu() {
		CMenu vMenuS = new CMenu('S', 20, 2, 200, 2, 150000);
		CMenu vMenuM = new CMenu('M', 25, 4, 300, 3, 200000);
		CMenu vMenuL = new CMenu('L', 30, 8, 500, 4, 250000);

		ArrayList<CMenu> vListMenu = new ArrayList<CMenu>();
		vListMenu.add(vMenuS);
		vListMenu.add(vMenuM);
		vListMenu.add(vMenuL);
		return vListMenu;
	}
}
