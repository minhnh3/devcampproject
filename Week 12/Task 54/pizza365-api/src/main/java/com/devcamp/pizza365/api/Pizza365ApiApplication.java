package com.devcamp.pizza365.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pizza365ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Pizza365ApiApplication.class, args);
	}

}
