package com.devcamp.pizza365.api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.api.drink.CDrink;

@RestController
public class CDrinkController {
	@CrossOrigin
	@GetMapping("/")
	public ArrayList<CDrink> getDrinkList() {
		CDrink vTraTac = new CDrink("TRATAC", "Trà tắc", 10000, null, 1615177934000L, 1615177934000L);
		CDrink vCoca = new CDrink("COCA", "Cocacola", 15000, null, 1615177934000L, 1615177934000L);
		CDrink vPepsi = new CDrink("PEPSI", "Pepsi", 15000, null, 1615177934000L, 1615177934000L);
		CDrink vLavie = new CDrink("LAVIE", "Lavie", 5000, null, 1615177934000L, 1615177934000L);
		CDrink vTraSua = new CDrink("TRASUA", "Trà sữa trân châu", 40000, null, 1615177934000L, 1615177934000L);
		CDrink vFanta = new CDrink("FANTA", "Fanta", 15000, null, 1615177934000L, 1615177934000L);

		ArrayList<CDrink> vDrinkList = new ArrayList<CDrink>();
		vDrinkList.add(vTraTac);
		vDrinkList.add(vCoca);
		vDrinkList.add(vPepsi);
		vDrinkList.add(vLavie);
		vDrinkList.add(vTraSua);
		vDrinkList.add(vFanta);
		return vDrinkList;
	}
}
