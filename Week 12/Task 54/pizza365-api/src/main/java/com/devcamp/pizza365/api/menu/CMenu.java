package com.devcamp.pizza365.api.menu;

public class CMenu {
	private char kichCo;
	private int duongKinh;
	private int suon;
	private int salad;
	private int soLuongDoUong;
	private int donGia;

	public CMenu(char kichCo, int duongKinh, int suon, int salad, int soLuongDoUong, int donGia) {
		super();
		this.kichCo = kichCo;
		this.duongKinh = duongKinh;
		this.suon = suon;
		this.salad = salad;
		this.soLuongDoUong = soLuongDoUong;
		this.donGia = donGia;
	}

	public char getKichCo() {
		return kichCo;
	}

	public void setKichCo(char kichCo) {
		this.kichCo = kichCo;
	}

	public int getDuongKinh() {
		return duongKinh;
	}

	public void setDuongKinh(int duongKinh) {
		this.duongKinh = duongKinh;
	}

	public int getSuon() {
		return suon;
	}

	public void setSuon(int suon) {
		this.suon = suon;
	}

	public int getSalad() {
		return salad;
	}

	public void setSalad(int salad) {
		this.salad = salad;
	}

	public int getSoLuongDoUong() {
		return soLuongDoUong;
	}

	public void setSoLuongDoUong(int soLuongDoUong) {
		this.soLuongDoUong = soLuongDoUong;
	}

	public int getDonGia() {
		return donGia;
	}

	public void setDonGia(int donGia) {
		this.donGia = donGia;
	}

	@Override
	public String toString() { // TODO Auto-generated method stub
		return "{kichCo:'" + this.kichCo + "', duongKinh:" + this.duongKinh + ", suon:" + this.suon + ", salad:"
				+ this.salad + ", soLuongDoUong:" + this.soLuongDoUong + ", donGia:" + this.donGia + "}";
	}

}
