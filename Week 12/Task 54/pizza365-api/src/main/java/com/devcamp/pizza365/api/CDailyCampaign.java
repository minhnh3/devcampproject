package com.devcamp.pizza365.api;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//@RestController
public class CDailyCampaign {
//	@CrossOrigin
//	@GetMapping
	public String getDailyCampaign() {
		DateTimeFormatter vVietnamDateFormat = DateTimeFormatter.ofPattern("EEEE")
				.localizedBy(Locale.forLanguageTag("vi"));
		LocalDate vToday = LocalDate.now();
		String vDailyCampaignContent = "";
		switch (vToday.format(vVietnamDateFormat)) {
		case "Thứ Hai":
			vDailyCampaignContent = "mua 1 tặng 1";
			break;
		case "Thứ Ba":
			vDailyCampaignContent = "giảm 100% pizza thứ 3";
			break;
		case "Thứ Tư":
			vDailyCampaignContent = "tặng nước";
			break;
		case "Thứ Năm":
			vDailyCampaignContent = "tặng người yêu";
			break;
		case "Thứ Sáu":
			vDailyCampaignContent = "giảm 50% đơn hàng mang về";
			break;
		case "Thứ Bảy":
			vDailyCampaignContent = "máu chảy về tim";
			break;
		case "Chủ Nhật":
			vDailyCampaignContent = "nghỉ không bán hàng";
			break;
		default:
			vDailyCampaignContent = "";
			break;
		}
		return String.format("%s, %s", vToday.format(vVietnamDateFormat), vDailyCampaignContent);
	}

}
