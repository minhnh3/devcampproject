/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gNONE_SELECTED = "NONE";
var gUserOrderObj = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    soLuongNuoc: "",
    thanhTien: "",
    idLoaiNuocUong: "",
    idVourcher: "",
    loaiPizza: "",
    hoTen: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
};
var gBtnSelectSizeS = $("#btn-select-size-s");
var gBtnSelectSizeM = $("#btn-select-size-m");
var gBtnSelectSizeL = $("#btn-select-size-l");
var gBtnSelectPizzaSeafood = $("#btn-select-pizza-seafood");
var gBtnSelectPizzaHawaii = $("#btn-select-pizza-hawaii");
var gBtnSelectPizzaBacon = $("#btn-select-pizza-bacon");

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
/* Make bootstrap navbar to push down the whole page on mobile menu */
$(document).ready(function () {
    var vSelectBody = $('body');
    var vSelectNavbarCollapse = $('.navbar-collapse');

    var vHeightNavbarCollapsed = $('.navbar').outerHeight(true);
    var vHeightNavbarExpanded = 0;

    paddingSmall();

    vSelectNavbarCollapse.on('show.bs.collapse', function () {
        if (vHeightNavbarExpanded == 0) {
            vHeightNavbarExpanded = vHeightNavbarCollapsed + $(this).outerHeight(true);
            paddingGreat();
        }
    })
    vSelectNavbarCollapse.on('hide.bs.collapse', function () {
        paddingSmall();
    })

    function paddingSmall() {
        vSelectBody.css('padding-top', vHeightNavbarCollapsed + 'px');
    }
    function paddingGreat() {
        vSelectBody.css('padding-top', vHeightNavbarExpanded + 'px');
    }
});
// thực thi sự kiện tải trang
onPageLoading();
// gán sự kiện khi ấn nút chọn combo pizza size S
gBtnSelectSizeS.on({
    click: function () {
        onBtnSelectSizeSClick();
    }
});
// gán sự kiện khi ấn nút chọn combo pizza size M
gBtnSelectSizeM.on({
    click: function () {
        onBtnSelectSizeMClick();
    }
});
// gán sự kiện khi ấn nút chọn combo pizza size L
gBtnSelectSizeL.on({
    click: function () {
        onBtnSelectSizeLClick();
    }
});
// gán sự kiện khi ấn nút chọn pizza seafood
gBtnSelectPizzaSeafood.on({
    click: function () {
        onBtnSelectPizzaSeafoodClick();
    }
});
// gán sự kiện khi ấn nút chọn pizza hawaii
gBtnSelectPizzaHawaii.on({
    click: function () {
        onBtnSelectPizzaHawaiiClick();
    }
});
// gán sự kiện khi ấn nút chọn pizza bacon
gBtnSelectPizzaBacon.on({
    click: function () {
        onBtnSelectPizzaBaconClick();
    }
});
// gán sự kiện khi ấn nút Gửi
$("#btn-send-order").on({
    click: function () {
        onBtnSendOrderClick();
    }
});
// gán sự kiện khi ấn nút Tạo đơn
$("#btn-create-order").on({
    click: function () {
        onBtnCreateOrderClick();
    }
});
// gán sự kiện khi ẩn Modal xác nhận tạo đơn hàng


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// xử lý sự kiện khi tải trang
// input/start: chưa có thông tin đồ uống
// output/end: có danh sách đồ uống để chọn
function onPageLoading() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate 
    // B3: gọi API  
    callAjaxApiGetDailyCampaign();
    callAjaxApiGetListMenu();
    callAjaxApiGetDrinkList();
    // B4: xử lý hiển thị
}
// xử lý sự kiện khi ấn nút chọn combo pizza size S
// input/start: 
// output/end: lưu combo pizza size S khách đã chọn
function onBtnSelectSizeSClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // lưu combo pizza size S khách đã chọn
    saveComboPizzaSToUserOrder();
    // B2: validate 
    // B3: gọi API  
    // B4: xử lý hiển thị
    // đổi màu hiển thị nút được chọn 
    changeColorBtnSelectComboSize();
}
// xử lý sự kiện khi ấn nút chọn combo pizza size M
// input/start: 
// output/end: lưu combo pizza size M khách đã chọn
function onBtnSelectSizeMClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // lưu combo pizza size M khách đã chọn
    saveComboPizzaMToUserOrder();
    // B2: validate 
    // B3: gọi API  
    // B4: xử lý hiển thị
    // đổi màu hiển thị nút được chọn 
    changeColorBtnSelectComboSize();
}
// xử lý sự kiện khi ấn nút chọn combo pizza size L
// input/start: 
// output/end: lưu combo pizza size L khách đã chọn
function onBtnSelectSizeLClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // lưu combo pizza size L khách đã chọn
    saveComboPizzaLToUserOrder();
    // B2: validate 
    // B3: gọi API  
    // B4: xử lý hiển thị
    // đổi màu hiển thị nút được chọn 
    changeColorBtnSelectComboSize();
}
// xử lý sự kiện khi ấn nút chọn pizza seafood
// input/start: 
// output/end: lưu loại pizza seafood khách đã chọn
function onBtnSelectPizzaSeafoodClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // lưu combo pizza seafood khách đã chọn
    savePizzaSeafoodToUserOrder();
    // B2: validate 
    // B3: gọi API  
    // B4: xử lý hiển thị
    // đổi màu hiển thị nút được chọn 
    changeColorBtnSelectPizzaType();
}
// xử lý sự kiện khi ấn nút chọn pizza hawaii
// input/start: 
// output/end: lưu loại pizza hawaii khách đã chọn
function onBtnSelectPizzaHawaiiClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // lưu combo pizza hawaii khách đã chọn
    savePizzaHawaiiToUserOrder();
    // B2: validate 
    // B3: gọi API  
    // B4: xử lý hiển thị
    // đổi màu hiển thị nút được chọn 
    changeColorBtnSelectPizzaType();
}
// xử lý sự kiện khi ấn nút chọn pizza bacon
// input/start: 
// output/end: lưu loại pizza bacon khách đã chọn
function onBtnSelectPizzaBaconClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // lưu combo pizza bacon khách đã chọn
    savePizzaBaconToUserOrder();
    // B2: validate 
    // B3: gọi API  
    // B4: xử lý hiển thị
    // đổi màu hiển thị nút được chọn 
    changeColorBtnSelectPizzaType();
}
// xử lý sự kiện khi ấn nút gửi kiểm tra voucher và hiển thị modal thông tin đơn hàng xác nhận
function onBtnSendOrderClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    getUserInfo();
    // B2: validate 
    var vValidateStatus = validateUserOrder();
    // B3: gọi API  
    if (vValidateStatus) {
        callApiAjaxCheckVoucherId();
        $("#modal-confirm-order").modal();
    }
    // B4: xử lý hiển thị
}
// xử lý sự kiện khi ấn nút Tạo đơn
function onBtnCreateOrderClick() {
    // B0: tạo đối tượng chứa dữ liệu
    // B1: thu thập dữ liệu
    // B2: validate 
    // B3: gọi API  
    callApiAjaxCreateOrder();
    // B4: xử lý hiển thị
    $("#modal-confirm-order").modal("hide");
    $("#modal-order-result").modal();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// gọi API lấy thông tin nội dung banner khuyến mại
function callAjaxApiGetDailyCampaign() {
    $.ajax({
        url: "http://localhost:8080/campaigns/",
        type: "GET",
        success: function (resDailyCampaign) {
            loadDailyCampaign(resDailyCampaign);
        },
        error: function (err) {
            console.log(err.status);
        }
    });
}
// load thông tin chương trình khuyến mại ngày
function loadDailyCampaign(paramResDailyCampaign) {
    $("#span-daily-campaign").text(paramResDailyCampaign);
}
// gọi API lấy thông tin danh sách các combo
function callAjaxApiGetListMenu() {
    $.ajax({
        url: "http://localhost:8080/combomenu/",
        type: "GET",
        success: function (resListMenu) {
            loadListMenu(resListMenu);
        },
        error: function (err) {
            console.log(err.status);
        }
    });
}
// load danh sách các combo vào panel
function loadListMenu(paramResListMenu) {
    for (var bIndex = 0; bIndex < paramResListMenu.length; bIndex++) {
        if (paramResListMenu[bIndex].size == "S") {
            $("#div-kich-co-combo-s").text(paramResListMenu[bIndex].size + " (small)");
            $("#span-duong-kinh-combo-s").text(paramResListMenu[bIndex].duongKinh + "cm");
            $("#span-suon-combo-s").text(paramResListMenu[bIndex].suon);
            $("#span-salad-combo-s").text(paramResListMenu[bIndex].salad + "g");
            $("#span-so-luong-do-uong-combo-s").text(paramResListMenu[bIndex].soLuongDoUong);
            $("#p-don-gia-combo-s").text(new Intl.NumberFormat().format(paramResListMenu[bIndex].donGia));
        }
        if (paramResListMenu[bIndex].size == "M") {
            $("#div-kich-co-combo-m").text(paramResListMenu[bIndex].size + " (medium)");
            $("#span-duong-kinh-combo-m").text(paramResListMenu[bIndex].duongKinh + "cm");
            $("#span-suon-combo-m").text(paramResListMenu[bIndex].suon);
            $("#span-salad-combo-m").text(paramResListMenu[bIndex].salad + "g");
            $("#span-so-luong-do-uong-combo-m").text(paramResListMenu[bIndex].soLuongDoUong);
            $("#p-don-gia-combo-m").text(new Intl.NumberFormat().format(paramResListMenu[bIndex].donGia));
        }
        if (paramResListMenu[bIndex].size == "L") {
            $("#div-kich-co-combo-l").text(paramResListMenu[bIndex].size + " (large)");
            $("#span-duong-kinh-combo-l").text(paramResListMenu[bIndex].duongKinh + "cm");
            $("#span-suon-combo-l").text(paramResListMenu[bIndex].suon);
            $("#span-salad-combo-l").text(paramResListMenu[bIndex].salad + "g");
            $("#span-so-luong-do-uong-combo-l").text(paramResListMenu[bIndex].soLuongDoUong);
            $("#p-don-gia-combo-l").text(new Intl.NumberFormat().format(paramResListMenu[bIndex].donGia));
        }
    }
}
// lưu combo pizza size S khách đã chọn
// input/start: thông tin combo pizza size S chưa lưu vào đối tượng
// output/end: thông tin combo pizza size S đã lưu vào đối tượng
function saveComboPizzaSToUserOrder() {
    gUserOrderObj.kichCo = "S";
    gUserOrderObj.duongKinh = "20 cm";
    gUserOrderObj.suon = "2";
    gUserOrderObj.salad = "200 g";
    gUserOrderObj.soLuongNuoc = "2";
    gUserOrderObj.thanhTien = "150000";
}
// lưu combo pizza size M khách đã chọn
// input/start: thông tin combo pizza size M chưa lưu vào đối tượng
// output/end: thông tin combo pizza size M đã lưu vào đối tượng
function saveComboPizzaMToUserOrder() {
    gUserOrderObj.kichCo = "M";
    gUserOrderObj.duongKinh = "25 cm";
    gUserOrderObj.suon = "4";
    gUserOrderObj.salad = "300 g";
    gUserOrderObj.soLuongNuoc = "3";
    gUserOrderObj.thanhTien = "200000";
}
// lưu combo pizza size L khách đã chọn
// input/start: thông tin combo pizza size L chưa lưu vào đối tượng
// output/end: thông tin combo pizza size L đã lưu vào đối tượng
function saveComboPizzaLToUserOrder() {
    gUserOrderObj.kichCo = "L";
    gUserOrderObj.duongKinh = "30 cm";
    gUserOrderObj.suon = "8";
    gUserOrderObj.salad = "500 g";
    gUserOrderObj.soLuongNuoc = "4";
    gUserOrderObj.thanhTien = "250000";
}
// đổi màu hiển thị các nút combo pizza size 
// input/start: 
// output/end: 
function changeColorBtnSelectComboSize() {
    if (gUserOrderObj.kichCo == "S") {
        gBtnSelectSizeS.removeClass("bg-orange").addClass("bg-gold");
        gBtnSelectSizeM.removeClass("bg-gold").addClass("bg-orange");
        gBtnSelectSizeL.removeClass("bg-gold").addClass("bg-orange");
    }
    if (gUserOrderObj.kichCo == "M") {
        gBtnSelectSizeS.removeClass("bg-gold").addClass("bg-orange");
        gBtnSelectSizeM.removeClass("bg-orange").addClass("bg-gold");
        gBtnSelectSizeL.removeClass("bg-gold").addClass("bg-orange");
    }
    if (gUserOrderObj.kichCo == "L") {
        gBtnSelectSizeS.removeClass("bg-gold").addClass("bg-orange");
        gBtnSelectSizeM.removeClass("bg-gold").addClass("bg-orange");
        gBtnSelectSizeL.removeClass("bg-orange").addClass("bg-gold");
    }
    console.log(gUserOrderObj);
}
// lưu loại pizza seafood khách đã chọn
// input/start: thông tin loại pizza seafood chưa lưu vào đối tượng
// output/end: thông tin loại pizza seafood đã lưu vào đối tượng
function savePizzaSeafoodToUserOrder() {
    gUserOrderObj.loaiPizza = "seafood";
}
// lưu loại pizza hawaii khách đã chọn
// input/start: thông tin loại pizza hawaii chưa lưu vào đối tượng
// output/end: thông tin loại pizza hawaii đã lưu vào đối tượng
function savePizzaHawaiiToUserOrder() {
    gUserOrderObj.loaiPizza = "hawaii";
}
// lưu loại pizza bacon khách đã chọn
// input/start: thông tin loại pizza bacon chưa lưu vào đối tượng
// output/end: thông tin loại pizza bacon đã lưu vào đối tượng
function savePizzaBaconToUserOrder() {
    gUserOrderObj.loaiPizza = "bacon";
}
// đổi màu hiển thị các nút chọn loại pizza
// input/start: 
// output/end: 
function changeColorBtnSelectPizzaType() {
    if (gUserOrderObj.loaiPizza == "seafood") {
        gBtnSelectPizzaSeafood.removeClass("bg-orange").addClass("bg-gold");
        gBtnSelectPizzaHawaii.removeClass("bg-gold").addClass("bg-orange");
        gBtnSelectPizzaBacon.removeClass("bg-gold").addClass("bg-orange");
    }
    if (gUserOrderObj.loaiPizza == "hawaii") {
        gBtnSelectPizzaSeafood.removeClass("bg-gold").addClass("bg-orange");
        gBtnSelectPizzaHawaii.removeClass("bg-orange").addClass("bg-gold");
        gBtnSelectPizzaBacon.removeClass("bg-gold").addClass("bg-orange");
    }
    if (gUserOrderObj.loaiPizza == "bacon") {
        gBtnSelectPizzaSeafood.removeClass("bg-gold").addClass("bg-orange");
        gBtnSelectPizzaHawaii.removeClass("bg-gold").addClass("bg-orange");
        gBtnSelectPizzaBacon.removeClass("bg-orange").addClass("bg-gold");
    }
    console.log(gUserOrderObj);
}
// gọi API lấy list đồ uống
// input/start: chưa có danh sách đồ uống
// output/end: lấy được danh sách đồ uống để load vào mục chọn đồ uống
function callAjaxApiGetDrinkList() {
    $.ajax({
        async: false,
        url: "http://localhost:8080/drinks/",
        type: "GET",
        success: function (resDrinkList) {
            loadDrinkListToSelect(resDrinkList);
        },
        error: function (err) {
            console.log(err.status);
        }
    });
}
// load list đồ uống
// input/start: paramResDrinkList danh sách đồ uống server trả về
// output/end: load danh sách vào ô chọn đồ uống
function loadDrinkListToSelect(paramResDrinkList) {
    var vSelectLoaiNuocUongSelector = $("#select-loai-nuoc-uong");
    // xoá các option ban đầu
    vSelectLoaiNuocUongSelector.find("option").remove();
    // thêm option chọn nước uống
    vSelectLoaiNuocUongSelector.append($("<option/>", {
        value: gNONE_SELECTED,
        text: "--- Chọn một loại đồ uống ---"
    }));
    // thêm các option đồ uống
    for (var bI = 0; bI < paramResDrinkList.length; bI++) {
        $("<option/>", {
            value: paramResDrinkList[bI].maNuocUong,
            text: paramResDrinkList[bI].tenNuocUong
        }).appendTo(vSelectLoaiNuocUongSelector);
    }
}
// lấy các thông tin khác người dùng nhập vào
function getUserInfo() {
    // chuẩn hoá
    $("#inp-ho-ten").val($("#inp-ho-ten").val().trim());
    $("#inp-email").val($("#inp-email").val().trim());
    $("#inp-so-dien-thoai").val($("#inp-so-dien-thoai").val().trim());
    $("#inp-dia-chi").val($("#inp-dia-chi").val().trim());
    $("#inp-id-vourcher").val($("#inp-id-vourcher").val().trim());
    $("#inp-loi-nhan").val($("#inp-loi-nhan").val().trim());
    // lấy thông tin vào đối tượng
    gUserOrderObj.idLoaiNuocUong = $("#select-loai-nuoc-uong").val();
    gUserOrderObj.hoTen = $("#inp-ho-ten").val();
    gUserOrderObj.email = $("#inp-email").val();
    gUserOrderObj.soDienThoai = $("#inp-so-dien-thoai").val();
    gUserOrderObj.diaChi = $("#inp-dia-chi").val();
    gUserOrderObj.idVourcher = $("#inp-id-vourcher").val();
    gUserOrderObj.loiNhan = $("#inp-loi-nhan").val();
}
// validate thông tin order
function validateUserOrder() {
    if (gUserOrderObj.kichCo.length == 0) {
        alert("Hãy chọn combo size pizza !!!");
        $('html, body').animate({
            scrollTop: $("#area-select-combo-pizza-size").offset().top
        }, 500);
        return false;
    }
    if (gUserOrderObj.loaiPizza.length == 0) {
        alert("Hãy chọn loại pizza !!!");
        $('html, body').animate({
            scrollTop: $("#area-select-pizza-type").offset().top
        }, 500);
        return false;
    }
    if (gUserOrderObj.idLoaiNuocUong == gNONE_SELECTED || gUserOrderObj.idLoaiNuocUong == "") {
        alert("Hãy chọn đồ uống !!!");
        $('html, body').animate({
            scrollTop: $("#area-select-loai-nuoc-uong").offset().top
        }, 500);
        $("#select-loai-nuoc-uong").focus();
        return false;
    }
    if (gUserOrderObj.hoTen.length == 0) {
        alert("Hãy điền thông tin Tên !!!");
        $("#inp-ho-ten").focus();
        return false;
    }
    if (gUserOrderObj.email.length == 0) {
        alert("Hãy điền thông tin Email !!!");
        $("#inp-email").focus();
        return false;
    }
    if (!validateEmail(gUserOrderObj.email)) {
        alert("Thông tin Email không đúng định dạng !!!");
        $("#inp-email").focus();
        return false;
    }
    if (gUserOrderObj.soDienThoai.length == 0) {
        alert("Hãy điền thông tin Số điện thoại !!!");
        $("#inp-so-dien-thoai").focus();
        return false;
    }
    if (isNaN(gUserOrderObj.soDienThoai)) {
        alert("Số điện thoại không đúng định dạng !!!");
        $("#inp-so-dien-thoai").focus();
        return false;
    }
    if (gUserOrderObj.diaChi.length == 0) {
        alert("Hãy điền thông tin Địa chỉ !!!");
        $("#inp-dia-chi").focus();
        return false;
    }
    return true;
    // validate email
    function validateEmail(paramEmail) {
        const vRE = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return vRE.test(String(paramEmail).toLowerCase());
    }
}
//gọi API kiểm tra thông tin voucher
function callApiAjaxCheckVoucherId() {
    $.ajax({
        async: false,
        url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + gUserOrderObj.idVourcher,
        type: "GET",
        success: function (resCheckVoucherIdObj) {
            var vDiscount = resCheckVoucherIdObj.phanTramGiamGia;
            loadOrderInfoToModal(vDiscount);
        },
        error: function (err) {
            console.log(err.status);
            var vDiscount = 0;
            loadOrderInfoToModal(vDiscount);
        }
    });
}
// load thông tin order lên modal xác nhận đơn hàng
function loadOrderInfoToModal(paramDiscount) {
    $("#modal-inp-ho-ten").val(gUserOrderObj.hoTen);
    $("#modal-inp-so-dien-thoai").val(gUserOrderObj.soDienThoai);
    $("#modal-inp-dia-chi").val(gUserOrderObj.diaChi);
    $("#modal-inp-loi-nhan").val(gUserOrderObj.loiNhan);
    $("#modal-inp-id-vourcher").val(gUserOrderObj.idVourcher);
    var vTotal = -1;
    if (paramDiscount !== 0) {
        vTotal = gUserOrderObj.thanhTien * (100 - paramDiscount) / 100;
    } else {
        vTotal = gUserOrderObj.thanhTien;
    }
    var vUserOrderInfoText =
        "* Menu combo size " + gUserOrderObj.kichCo +
        ", đường kính " + gUserOrderObj.duongKinh +
        ", sườn " + gUserOrderObj.suon +
        ", salad " + gUserOrderObj.salad +
        ", loại nước uống " + $("#select-loai-nuoc-uong option[value='" + gUserOrderObj.idLoaiNuocUong + "']").text() +
        ", số lượng nước " + gUserOrderObj.soLuongNuoc +
        "\r\n" +
        "* Loại pizza lựa chọn: " + gUserOrderObj.loaiPizza +
        "\r\n" +
        "* Giá: " + new Intl.NumberFormat().format(gUserOrderObj.thanhTien) + " VNĐ" +
        "\r\n" +
        "* Discount: " + paramDiscount + " % " +
        "\r\n" +
        "* Phải thanh toán: " + new Intl.NumberFormat().format(vTotal) + " VNĐ";
    $("#modal-textarea-order-detail").val(vUserOrderInfoText);
}
// gọi API tạo đơn hàng
function callApiAjaxCreateOrder() {
    $.ajax({
        url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(gUserOrderObj),
        success: function (resUserOrderObj) {
            var vUserOrderObj = resUserOrderObj;
            loadModalOrderResult(vUserOrderObj.orderId);
        },
        error: function (err) {
            console.log(err.status);
            loadModalOrderResult(vUserOrderObj.orderId);
        }
    });
}
// load thông tin order lên modal kết quả đơn hàng được tạo
function loadModalOrderResult(paramOrderId) {
    $("#modal-inp-order-id").val(paramOrderId);
}