package com.devcamp.task5220;

public interface IBarkable {
	public String bark();
}
