package com.devcamp.task5220;

public class CCat extends CPet implements IRunable {
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Cat running");
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		System.out.println("=== Cat information ===");
		System.out.println("Animal Class: " + this.animalClass);
		System.out.println("Cat age = " + this.age);
		System.out.println("Cat name = " + this.name);
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("sleeping...");
	}

	@Override
	public String animalSound() {
		// TODO Auto-generated method stub
		return super.animalSound();
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		super.eat();
		System.out.println("Cat eat");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{Animal Class:" + this.animalClass + ", age:" + this.age + ", name:" + this.name + "}";
	}

	public CCat() {
		// TODO Auto-generated constructor stub
	}

	public CCat(int age, String name) {
		super(age, name);
		// TODO Auto-generated constructor stub
	}

	public CCat(AnimalClass animalClass, int age, String name) {
		super(animalClass, age, name);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		CCat vCat = new CCat(AnimalClass.MAMMALS, 2, "meo");
		vCat.play();
		vCat.print();
		System.out.println("===");
		CPet vCatPet = new CCat(AnimalClass.MAMMALS, 22, "miao");
		vCatPet.play();
		vCatPet.print();
	}

}
