package com.devcamp.task5220;

public class CDog extends CPet implements IRunable, IBarkable {
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Dog running");
	}

	@Override
	public String bark() {
		// TODO Auto-generated method stub
		return "gau gau...";
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		System.out.println("=== Dog information ===");
		System.out.println("Animal Class: " + this.animalClass);
		System.out.println("Dog age = " + this.age);
		System.out.println("Dog name = " + this.name);
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("gauing...");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{Animal Class:" + this.animalClass + ", age:" + this.age + ", name:" + this.name + "}";
	}

	public CDog() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CDog(int age, String name) {
		super(age, name);
		// TODO Auto-generated constructor stub
	}

	public CDog(AnimalClass animalClass, int age, String name) {
		super(animalClass, age, name);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		CDog vDog = new CDog(AnimalClass.MAMMALS, 3, "gau");
		vDog.bark();
		vDog.play();
		vDog.print();
		System.out.println("===");
		CPet vDogPet = new CDog(AnimalClass.MAMMALS, 33, "gaow");
		vDogPet.play();
		vDogPet.print();
	}

}
