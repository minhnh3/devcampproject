package com.devcamp.task5220;

public abstract class CAnimal {
	public AnimalClass animalClass;

	public CAnimal() {
		super();
	}

	public CAnimal(AnimalClass animalClass) {
		super();
		this.animalClass = animalClass;
	}

	public abstract String animalSound();

	public abstract void eat();

}
