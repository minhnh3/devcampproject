package com.devcamp.task5220;

public enum AnimalClass {
	FISH, AMPHIBIANS, REPTILES, MAMMALS, BIRDS
}
