package com.devcamp.task5220;

public class CPet extends CAnimal {
	protected int age;
	protected String name;

	public CPet() {
		super();
	}

	public CPet(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}

	public CPet(AnimalClass animalClass, int age, String name) {
		super(animalClass);
		this.age = age;
		this.name = name;
	}

	protected void print() {
		System.out.println("=== Pet information ===");
		System.out.println("Animal Class: " + this.animalClass);
		System.out.println("Age: " + this.age);
		System.out.println("Name: " + this.name);
	}

	protected void play() {
		System.out.println("Pet playing");
	}

	@Override
	public String animalSound() {
		// TODO Auto-generated method stub
		return "Pet saying";
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Pet eat");
	}

}
