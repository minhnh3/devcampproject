package com.devcamp.task5220;

import com.devcamp.task5230.CBird;
import com.devcamp.task5230.CFish;

public class CMaintask5220 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AnimalClass vAm = AnimalClass.AMPHIBIANS;
		System.out.println(vAm);

		CPet vPet = new CPet(AnimalClass.BIRDS, 1, "jo");
		System.out.println(vPet.age);
		System.out.println(vPet.name);
		System.out.println(vPet.animalClass);
		vPet.print();
		vPet.play();
		System.out.println(vPet.animalSound());
		vPet.eat();

		System.out.println("=====");
		vPet = new CDog(3, "dogdog");
		vPet.print();
		vPet.play();
		System.out.println(vPet.animalSound());
		vPet.eat();
		System.out.println("-----");
		vPet = new CCat(2, "catcat");
		vPet.print();
		vPet.play();
		System.out.println(vPet.animalSound());
		vPet.eat();

		System.out.println("=====");
		CCat vCat = new CCat(AnimalClass.MAMMALS, 22, "catcatcatcat");
		vCat.print();
		vCat.play();
		System.out.println(vCat.animalSound());
		vCat.eat();
		vCat.run();
		System.out.println("-----");
		CDog vDog = new CDog(AnimalClass.MAMMALS, 333, "dogdogdog");
		vDog.print();
		vDog.play();
		System.out.println(vDog.animalSound());
		vDog.eat();
		vDog.run();
		System.out.println(vDog.bark());

		System.out.println("=====");
		CPerson vMinh = new CPerson(007, 30, "Minh", "Nguyen", "VN");
		vMinh.printPerson();
		System.out.println("-----");
		vMinh.addPet(vPet);
		vMinh.addPet(vCat);
		vMinh.addPet(vDog);
		vMinh.printPerson();
		System.out.println("-----");
		vMinh.removePet(0);
		vMinh.printPerson();

		System.out.println("");
		System.out.println("***** task5230 *****");
		CPet vBirdPet = new CBird(AnimalClass.BIRDS, 10, "bibi");
		System.out.println(vBirdPet.age);
		System.out.println(vBirdPet.name);
		System.out.println(vBirdPet.animalClass);
		vBirdPet.print();
		vBirdPet.play();
		System.out.println(vBirdPet.animalSound());
		vBirdPet.eat();
		System.out.println("-----");
		CFish vFish = new CFish(AnimalClass.FISH, 20, "fifi");
		System.out.println(vFish.age);
		System.out.println(vFish.name);
		System.out.println(vFish.animalClass);
		vFish.print();
		vFish.play();
		System.out.println(vFish.animalSound());
		vFish.eat();
		vFish.swim();
		System.out.println("-----");
		vMinh.addPet(vBirdPet);
		vMinh.addPet(vFish);
		vMinh.printPerson();
		System.out.println("-----");
		vMinh.removePet(0);
		vMinh.printPerson();
	}

}
