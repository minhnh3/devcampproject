package com.devcamp.task5240;

import java.util.ArrayList;

public class CIntegerArrayList implements ISumable {
	private ArrayList<Integer> mIntegerArrayList;

	public CIntegerArrayList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
		super();
		this.mIntegerArrayList = mIntegerArrayList;
	}

	public ArrayList<Integer> getmIntegerArrayList() {
		return mIntegerArrayList;
	}

	public void setmIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
		this.mIntegerArrayList = mIntegerArrayList;
	}

	@Override
	public int getSum() {
		// TODO Auto-generated method stub
		int vSum = 0;
		for (int i = 0; i < mIntegerArrayList.size(); i++) {
			vSum += mIntegerArrayList.get(i);
		}
		return vSum;
	}

	public static void main(String[] args) {
		CIntegerArrayList vIntegerArrayListObj = new CIntegerArrayList();
		ArrayList<Integer> vIntegerArrayList = new ArrayList<Integer>();
		vIntegerArrayList.add(0);
		vIntegerArrayList.add(1);
		vIntegerArrayList.add(2);
		vIntegerArrayList.add(3);
		vIntegerArrayList.add(4);
		vIntegerArrayList.add(5);
		vIntegerArrayListObj.setmIntegerArrayList(vIntegerArrayList);
		System.out.println(vIntegerArrayListObj.getSum());
	}

}
