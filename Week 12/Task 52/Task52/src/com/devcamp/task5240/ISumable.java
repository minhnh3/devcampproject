package com.devcamp.task5240;

public interface ISumable {
	public int getSum();
}
