package com.devcamp.task5240;

public class CIntArray implements ISumable {
	private int[] mIntArray;

	public CIntArray() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CIntArray(int[] mIntArray) {
		super();
		this.mIntArray = mIntArray;
	}

	public int[] getmIntArray() {
		return mIntArray;
	}

	public void setmIntArray(int[] mIntArray) {
		this.mIntArray = mIntArray;
	}

	@Override
	public int getSum() {
		// TODO Auto-generated method stub
		int vSum = 0;
		for (int i = 0; i < mIntArray.length; i++) {
			vSum += mIntArray[i];
		}
		return vSum;
	}

	public static void main(String[] args) {
		CIntArray vIntArrayObj = new CIntArray();
		int[] vIntArray = { 1, 2, 3, 4 };
		vIntArrayObj.setmIntArray(vIntArray);
		System.out.println(vIntArrayObj.getSum());
	}

}
