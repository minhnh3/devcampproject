package com.devcamp.task5240;

import java.util.ArrayList;

public class CMain {
	public static void testSumable(ISumable paramISum) {
		System.out.println(paramISum.getSum());
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CIntArray vIntArrayObj = new CIntArray();
		int[] vIntArray = { 1, 2, 3, 4 };
		vIntArrayObj.setmIntArray(vIntArray);
		testSumable(vIntArrayObj);

		System.out.println("===");

		CIntegerArrayList vIntegerArrayListObj = new CIntegerArrayList();
		ArrayList<Integer> vIntegerArrayList = new ArrayList<Integer>();
		vIntegerArrayList.add(0);
		vIntegerArrayList.add(1);
		vIntegerArrayList.add(2);
		vIntegerArrayList.add(3);
		vIntegerArrayList.add(4);
		vIntegerArrayList.add(5);
		vIntegerArrayListObj.setmIntegerArrayList(vIntegerArrayList);
		testSumable(vIntegerArrayListObj);
	}
}
