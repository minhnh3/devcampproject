package com.devcamp.task5230;

import com.devcamp.task5220.AnimalClass;
import com.devcamp.task5220.CPet;

public class CFish extends CPet implements ISwimable {

	public CFish() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CFish(AnimalClass animalClass, int age, String name) {
		super(animalClass, age, name);
		// TODO Auto-generated constructor stub
	}

	public CFish(int age, String name) {
		super(age, name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Fish swimming");
	}

}
