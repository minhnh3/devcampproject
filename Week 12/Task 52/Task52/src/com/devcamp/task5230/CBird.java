package com.devcamp.task5230;

import com.devcamp.task5220.AnimalClass;
import com.devcamp.task5220.CPet;

public class CBird extends CPet implements IFlyable {

	public CBird() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CBird(AnimalClass animalClass, int age, String name) {
		super(animalClass, age, name);
		// TODO Auto-generated constructor stub
	}

	public CBird(int age, String name) {
		super(age, name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Bird flying");
	}

}
