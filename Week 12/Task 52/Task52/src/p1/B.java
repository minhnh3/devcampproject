package p1;

public class B {
	public static void main(String[] args) {
		A aObj = new A();
//		aObj.i = 1; // private => KHÔNG TRUY XUẤT ĐƯỢC
		aObj.j = 10; // default
		aObj.k = 100; // protected
		aObj.l = 1000; // public
	}
}
