package p2;

import p1.A;

public class C extends A {
	public static void main(String[] args) {
		C cObj = new C();
//		cObj.i = 1; // private => KHÔNG TRUY XUẤT ĐƯỢC
//		cObj.j = 10; // default => KHÔNG TRUY XUẤT ĐƯỢC
		cObj.k = 100; // protected
		cObj.l = 1000; // public
	}
}
