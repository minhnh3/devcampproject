package com.devcamp;

import java.util.ArrayList;
import java.util.Iterator;

public class CPerson {
	private int id;
	private int age;
	private String firstName;
	private String lastName;
	public String country;
	private ArrayList<String> cars;
	private ArrayList<String> pets;

	public CPerson() {
		super();
		this.id = -1;
		this.age = -1;
		this.firstName = "";
		this.lastName = "";
		this.country = "";
		this.cars = new ArrayList<String>();
		this.pets = new ArrayList<String>();
	}

	public CPerson(int id, int age, String firstName, String lastName, String country) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.country = country;
		this.cars = new ArrayList<String>();
		this.pets = new ArrayList<String>();
	}

	public CPerson(int id, int age, String firstName, String lastName, String country, ArrayList<String> cars) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.country = country;
		this.cars = cars;
		this.pets = new ArrayList<String>();
	}

	public CPerson(int id, int age, String firstName, String lastName, String country, ArrayList<String> cars,
			ArrayList<String> pets) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.country = country;
		this.cars = cars;
		this.pets = pets;
	}

	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	public void addCars(ArrayList<String> paramCars) {
		this.cars = paramCars;
	}

	public void addCar(String paramCar) {
		this.cars.add(paramCar);
	}

	private void printAllCars() {
		System.out.println("Danh sách car:");
		for (String bElement : cars) {
			System.out.println("- " + bElement);
		}
	}

	public void addPets(ArrayList<String> paramPets) {
		this.pets = paramPets;
	}

	public void addPet(String paramPet) {
		this.pets.add(paramPet);
	}

	public void removePetByIndex(int paramIndex) {
		this.pets.remove(paramIndex);
	}

	private void printAllPets() {
		System.out.println("Danh sách pet:");
		for (int bI = 0; bI < pets.size(); bI++) {
			System.out.println("- " + pets.get(bI));
		}
	}

	public void printPerson() {
		System.out.println("id = " + this.id);
		System.out.println("age = " + this.age);
		System.out.println("firstName = " + this.firstName);
		System.out.println("lastName = " + this.lastName);
		System.out.println("country = " + this.country);
		System.out.println("cars = " + this.cars);
		System.out.println("pets = " + this.pets);
		printAllCars();
		printAllPets();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFirstName() {
		return firstName.toUpperCase();
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName.toUpperCase();
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public ArrayList<String> getCars() {
		return cars;
	}

	public void setCars(ArrayList<String> cars) {
		this.cars = cars;
	}

}
