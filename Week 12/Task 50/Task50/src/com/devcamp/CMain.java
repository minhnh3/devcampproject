package com.devcamp;

import java.util.ArrayList; // import the ArrayList class

public class CMain {
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_RESET = "\u001B[0m";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(ANSI_GREEN + "=== Task 50.10 Test 1 ===" + ANSI_RESET);
		CPerson joe = new CPerson(1, 11, "joe", "do", "USA");
		System.out.println(joe.getFullName());
		System.out.println(ANSI_GREEN + "=== Task 50.10 Test 2 ===" + ANSI_RESET);
		ArrayList<String> automobiles = new ArrayList<String>();
		automobiles.add("Volvo");
		automobiles.add("BMW");
		automobiles.add("Ford");
		automobiles.add("Mazda");
		System.out.println(automobiles);
		System.out.println(ANSI_GREEN + "=== Task 50.10 Test 3 ===" + ANSI_RESET);
		joe.printPerson();
		System.out.println(ANSI_GREEN + "=== Task 50.10 Test 4 ===" + ANSI_RESET);
		joe.setAge(22);
		joe.country = "VN";
		joe.printPerson();
		System.out.println(ANSI_GREEN + "=== Task 50.10 Test 5 ===" + ANSI_RESET);
		automobiles.add("Vinfast");
		System.out.println(automobiles);
		System.out.println(ANSI_GREEN + "=== Task 50.10 Test 6 ===" + ANSI_RESET);
		joe.addCars(automobiles);
		joe.printPerson();
		System.out.println("---");
		joe.addCar("Toyota");
		joe.printPerson();
		System.out.println(ANSI_GREEN + "=== Task 50.10 Test 7 ===" + ANSI_RESET);
		joe.printPerson();
		System.out.println(ANSI_GREEN + "=== Task 50.20 Test 1 ===" + ANSI_RESET);
		CPerson maria = new CPerson(3, 33, "Maria", "Obama", "Japan");
		maria.printPerson();
		System.out.println(ANSI_GREEN + "=== Task 50.20 Test 2 ===" + ANSI_RESET);
		maria.addPet("dog");
		maria.addPet("cat");
		maria.printPerson();
		System.out.println(ANSI_GREEN + "=== Task 50.20 Test 3 ===" + ANSI_RESET);
		maria.addCar("Honda");
		maria.printPerson();
		System.out.println(ANSI_GREEN + "=== Task 50.20 Test 4 ===" + ANSI_RESET);
		System.out.println(automobiles);
		automobiles.remove(1);
		System.out.println(automobiles);
		System.out.println(ANSI_GREEN + "=== Task 50.20 Test 5 ===" + ANSI_RESET);
		maria.removePetByIndex(0);
		maria.printPerson();
	}

}
